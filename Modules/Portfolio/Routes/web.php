<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'),"middleware"=>["auth:web"]],function() {
    Route::resource('/portfolios', 'PortfolioController')->only('create','store','destroy','update','index','edit');
    Route::group(["prefix"=>'portfolios'], function () {
        Route::get('/gallery/{portfolio}', 'PortfolioController@gallery')->name('portfolio.gallery');
        Route::post('/gallery/store/{portfolio}', 'PortfolioController@galleryStore')->name('portfolio.gallery.store');
        Route::get('/gallery/destroy/{media}', 'PortfolioController@galleryDestroy')->name('portfolio.gallery.destroy');
    });

    Route::group(["prefix"=>'portfolio/questions'], function () {
        Route::get('/{portfolio}', 'PortfolioController@question')->name('portfolio.questions');
        Route::get('/create/{portfolio}', 'PortfolioController@questionCreate')->name('portfolio.question.create');
        Route::post('/store/{portfolio}', 'PortfolioController@questionStore')->name('portfolio.question.store');
        Route::delete('/destroy/{question}', 'PortfolioController@questionDestroy')->name('portfolio.question.destroy');
        Route::get('/edit/{portfolio}/{question}', 'PortfolioController@questionEdit')->name('portfolio.question.edit');
        Route::patch('/update/{question}', 'PortfolioController@questionUpdate')->name('portfolio.question.update');
    });

    Route::group(["prefix"=>'portfolio/categories'], function () {
        Route::get('/', 'PortfolioController@categories')->name('portfolio.categories');
        Route::get('/create', 'PortfolioController@categoryCreate')->name('portfolio.category.create');
        Route::post('/store', 'PortfolioController@categoryStore')->name('portfolio.category.store');
        Route::get('/edit/{category}', 'PortfolioController@categoryEdit')->name('portfolio.category.edit');
        Route::patch('/update/{category}', 'PortfolioController@categoryUpdate')->name('portfolio.category.update');

    });


    Route::group(["prefix"=>'search'], function () {
        Route::post('/portfolios', 'PortfolioController@search')->name('search.portfolio');
    });

});
