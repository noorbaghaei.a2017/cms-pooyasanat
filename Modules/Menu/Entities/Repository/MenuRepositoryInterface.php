<?php


namespace Modules\Menu\Entities\Repository;


interface MenuRepositoryInterface
{
    public function getAll();

}
