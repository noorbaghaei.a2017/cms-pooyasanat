<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'),"middleware"=>["auth:web"]],function() {
    Route::get('/races/student/list/{race}', 'RaceController@studentShow')->name('student.show.race');
    Route::get('/force/student/add/race/{race}', 'RaceController@studentForce')->name('student.show.race.force.add');
    Route::post('/force/student/register/race/{race}', 'RaceController@studentRegisterForce')->name('register.race.client.force');
    Route::delete('/force/student/destroy/race/{client}/{race}', 'RaceController@studentDestroyForce')->name('destroy.race.client.force');
    Route::get('/races/show/score/{data}/{token}', 'RaceController@showScore')->name('student.race.show.score');
    Route::post('/races/set/score/{data}/{token}', 'RaceController@registerScore')->name('student.race.register.score');
    Route::post('/races/set/multi/score/{data}/{token}', 'RaceController@registerScoreMulti')->name('student.race.register.score.multi');
    Route::resource('/races', 'RaceController');

    Route::get('/classrooms/student/list/{classroom}', 'ClassRoomController@studentShow')->name('student.show.classroom');
    Route::get('/force/student/add/classroom/{classroom}', 'ClassRoomController@studentForce')->name('student.show.classroom.force.add');
    Route::post('/force/student/register/classroom/{classroom}', 'ClassRoomController@studentRegisterForce')->name('register.classroom.client.force');
    Route::delete('/force/student/destroy/classroom/{client}/{classroom}', 'ClassRoomController@studentDestroyForce')->name('destroy.classroom.client.force');
    Route::get('/classrooms/show/score/{data}/{token}', 'ClassRoomController@showScore')->name('student.classroom.show.score');
    Route::post('/classrooms/set/score/{data}/{token}', 'ClassRoomController@registerScore')->name('student.classroom.register.score');
    Route::post('/classrooms/set/multi/score/{data}/{token}', 'ClassRoomController@registerScoreMulti')->name('student.classroom.register.score.multi');
    Route::resource('/classrooms', 'ClassRoomController');

    Route::group(["prefix"=>'search'], function () {
        Route::post('/classrooms', 'ClassRoomController@search')->name('search.classroom');
        Route::post('/races', 'RaceController@search')->name('search.race');
    });

    Route::group(["prefix"=>'race/categories'], function () {
        Route::get('/', 'RaceController@categories')->name('race.categories');
        Route::get('/create', 'RaceController@categoryCreate')->name('race.category.create');
        Route::post('/store', 'RaceController@categoryStore')->name('race.category.store');
        Route::get('/edit/{category}', 'RaceController@categoryEdit')->name('race.category.edit');
        Route::patch('/update/{category}', 'RaceController@categoryUpdate')->name('race.category.update');

    });


    Route::group(["prefix"=>'race/leaders'], function () {
        Route::get('/', 'RaceController@leaders')->name('race.leaders');
        Route::get('/create', 'RaceController@leaderCreate')->name('race.leader.create');
        Route::post('/store', 'RaceController@leaderStore')->name('race.leader.store');
        Route::get('/edit/{leader}', 'RaceController@leaderEdit')->name('race.leader.edit');
        Route::patch('/update/{leader}', 'RaceController@leaderUpdate')->name('race.leader.update');

    });


    Route::group(["prefix"=>'race/questions'], function () {
        Route::get('/{race}', 'RaceController@question')->name('race.questions');
        Route::get('/create/{race}', 'RaceController@questionCreate')->name('race.question.create');
        Route::post('/store/{race}', 'RaceController@questionStore')->name('race.question.store');
        Route::delete('/destroy/{question}', 'RaceController@questionDestroy')->name('race.question.destroy');
        Route::get('/edit/{race}/{question}', 'RaceController@questionEdit')->name('race.question.edit');
        Route::patch('/update/{question}', 'RaceController@questionUpdate')->name('race.question.update');
    });
    Route::group(["prefix"=>'races'], function () {
        Route::get('/gallery/{race}', 'RaceController@gallery')->name('race.gallery');
        Route::post('/gallery/store/{race}', 'RaceController@galleryStore')->name('race.gallery.store');
        Route::get('/gallery/destroy/{media}', 'RaceController@galleryDestroy')->name('race.gallery.destroy');
    });








    Route::group(["prefix"=>'classroom/categories'], function () {
        Route::get('/', 'ClassRoomController@categories')->name('classroom.categories');
        Route::get('/create', 'ClassRoomController@categoryCreate')->name('classroom.category.create');
        Route::post('/store', 'ClassRoomController@categoryStore')->name('classroom.category.store');
        Route::get('/edit/{category}', 'ClassRoomController@categoryEdit')->name('classroom.category.edit');
        Route::patch('/update/{category}', 'ClassRoomController@categoryUpdate')->name('classroom.category.update');

    });

    Route::group(["prefix"=>'classroom/leaders'], function () {
        Route::get('/', 'ClassRoomController@leaders')->name('classroom.leaders');
        Route::get('/create', 'ClassRoomController@leaderCreate')->name('classroom.leader.create');
        Route::post('/store', 'ClassRoomController@leaderStore')->name('classroom.leader.store');
        Route::get('/edit/{leader}', 'ClassRoomController@leaderEdit')->name('classroom.leader.edit');
        Route::patch('/update/{leader}', 'ClassRoomController@leaderUpdate')->name('classroom.leader.update');

    });

    Route::group(["prefix"=>'classroom/questions'], function () {
        Route::get('/{classroom}', 'ClassRoomController@question')->name('classroom.questions');
        Route::get('/create/{classroom}', 'ClassRoomController@questionCreate')->name('classroom.question.create');
        Route::post('/store/{classroom}', 'ClassRoomController@questionStore')->name('classroom.question.store');
        Route::delete('/destroy/{question}', 'ClassRoomController@questionDestroy')->name('classroom.question.destroy');
        Route::get('/edit/{educational}/{question}', 'ClassRoomController@questionEdit')->name('classroom.question.edit');
        Route::patch('/update/{question}', 'ClassRoomController@questionUpdate')->name('classroom.question.update');
    });
    Route::group(["prefix"=>'classrooms'], function () {
        Route::get('/gallery/{classroom}', 'ClassRoomController@gallery')->name('classroom.gallery');
        Route::post('/gallery/store/{classroom}', 'ClassRoomController@galleryStore')->name('classroom.gallery.store');
        Route::get('/gallery/destroy/{media}', 'ClassRoomController@galleryDestroy')->name('classroom.gallery.destroy');
    });


});
