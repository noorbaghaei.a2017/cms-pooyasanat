<?php

namespace Modules\Educational\Transformers;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Educational\Entities\Race;

class RaceCollection extends ResourceCollection
{
    public $collects = Race::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $item ) {
                    return new RaceResource($item);
                }
            ),
            'filed' => [
                'thumbnail','title','slug','status','price','category','count_student','count_child','view','like','questions','update_date','create_date'
            ],
            'public_route'=>[

                [
                    'name'=>'races.create',
                    'param'=>[null],
                    'icon'=>config('cms.icon.add'),
                    'title'=>__('cms.add'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                    'method'=>'GET',
                ],
                [
                    'name'=>'race.leaders',
                    'param'=>[null],
                    'icon'=>config('cms.icon.leaders'),
                    'title'=>__('cms.leaders'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-success pull-right',
                    'method'=>'GET',
                ],
                [
                    'name'=>'race.categories',
                    'param'=>[null],
                    'icon'=>config('cms.icon.categories'),
                    'title'=>__('cms.categories'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-warning pull-right',
                    'method'=>'GET',
                ]
            ],
            'private_route'=>
                [
                    [
                        'name'=>'races.edit',
                        'param'=>[
                            'race'=>'token'
                        ],
                        'icon'=>config('cms.icon.edit'),
                        'title'=>__('cms.edit'),
                        'class'=>'btn btn-warning btn-sm text-sm',
                        'modal'=>false,
                        'method'=>'GET',
                    ],
                    [
                        'name'=>'student.show.race',
                        'param'=>[
                            'race'=>'token'
                        ],
                        'icon'=>'fa fa-users',
                        'title'=>__('cms.students'),
                        'class'=>'btn btn-warning btn-sm text-sm',
                        'modal'=>false,
                        'method'=>'GET',
                    ],
                    [
                        'name'=>'races.destroy',
                        'param'=>[
                            'race'=>'token'
                        ],
                        'icon'=>config('cms.icon.delete'),
                        'title'=>__('cms.delete'),
                        'class'=>'btn btn-danger btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'destroy',
                        'method'=>'DELETE',
                    ],
                    [
                        'name'=>'races.detail',
                        'param'=>[null],
                        'icon'=>config('cms.icon.detail'),
                        'title'=>__('cms.detail'),
                        'class'=>'btn btn-primary btn-sm text-sm text-white',
                        'modal'=>true,
                        'modal_type'=>'detail',
                        'method'=>'GET',
                    ]
                ],
            'search_route'=>[

                'name'=>'search.race',
                'filter'=>[
                    'title','slug','price'
                ],
                'icon'=>config('cms.icon.add'),
                'title'=>__('cms.add'),
                'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                'method'=>'POST',

            ],
            'count' => $this->collection->count(),
            'links' => [
                'self' => 'link-value',
            ],
            'collect'=>__('educational::races.collect'),
            'title'=>__('educational::races.index'),
        ];
    }
}
