
    <li>
        <a href="{{route('articles.index')}}" class="b-success">
                              <span class="nav-icon text-white no-fade">
                                <i class="{{config('article.icons.article')}}"></i>
                              </span>

            <span class="nav-text">{{__('article::articles.collect')}}</span>
        </a>
    </li>

