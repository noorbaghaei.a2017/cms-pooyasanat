<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\Core\Entities\Award;

$factory->define(Award::class, function (Faker $faker) {
    return [
        'title'=>$faker->unique()->title,
        'symbol'=>$faker->text,
        'slug'=>$faker->slug,
    ];
});
