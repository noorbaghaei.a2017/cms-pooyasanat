<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Core\Entities\Country;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->delete();

        $states=[

            [
                'name' => 'Tehran',
                'country' => Country::where('name','Iran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'Hamedan',
                'country' => Country::where('name','Iran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'Karaj',
                'country' => Country::where('name','Iran')->first()->id,
                'created_at' =>now(),
            ],

        ];

        DB::table('states')->insert($states);
    }
}
