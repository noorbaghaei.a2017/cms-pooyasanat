<?php

return [
    "store"=>"ثبت با موفقیت انجام شد.",
    "send"=>"ارسال شد",
    "delete"=>"حذف با موفقیت انجام شد",
    "update"=>"بروز رسانی با موفقیت انجام شد",
    "index"=>"لیست پیام ها",
    "singular"=>"پیام",
    "collect"=>"پیام ها",
    "permission"=>[
        "brand-full-access"=>"دسترسی کامل به  پیام ها",
        "brand-list"=>"لیست پیام ها",
        "brand-delete"=>"حذف پیام",
    ]
];
