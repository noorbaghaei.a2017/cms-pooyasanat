<a title="{{$private->title}}" class="{{$private->class}}" data-toggle="modal" data-target="#modal-{{$values->token}}" data-ui-toggle-class="bounce" data-ui-target="#modal"><span class="{{$private->icon}}"></span></a>

<div id="modal-{{$values->token}}" class="modal fade animate" data-backdrop="true">
    <div class="modal-dialog" id="modal">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">  {{$values->title}}</h5>
            </div>
            <div class="modal-body text-center p-lg">
                <p>
                    {{__('cms.sure_delete')}}
                </p>
            </div>
            <div class="modal-footer">

                <form method="post" class="pull-right" action="{{route($private->name,convertArrayParam(collect($private->param)->toArray(),$values))}}">
                    @csrf
                    @method($private->method)
                    <button class="btn btn-danger p-x-md">{{__('cms.yes')}}</button>
                </form>
                <button type="button" class="btn dark-white p-x-md pull-left" data-dismiss="modal">
                    {{__('cms.cancel')}}</button>
            </div>
        </div>
    </div>
</div>
