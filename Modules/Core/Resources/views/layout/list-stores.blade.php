<div class="form-group row">
    @if(is_null($item))

        <div class="col-sm-12">
            <span class="text-danger">*</span>
            <label for="store" class="form-control-label">{{__('cms.store_category')}}  </label>
            <select dir="rtl" class="form-control" id="store" name="store" required>
                @foreach($stores as $store)
                    <option value="{{$store->token}}">{{$store->symbol}}</option>
                @endforeach

            </select>
        </div>

    @else

        <div class="form-group row">
            <div class="col-sm-12">
                <span class="text-danger">*</span>
                <label for="store" class="form-control-label">{{__('cms.store_category')}}  </label>
                <select dir="rtl" class="form-control" id="store" name="store" required>
                    @foreach($stores as $store)
                        <option value="{{$store->token}}" {{$store->id==$item->store_category ? "selected" : ""}}>{{$store->symbol}}</option>
                    @endforeach

                </select>
            </div>
        </div>

    @endif
</div>
