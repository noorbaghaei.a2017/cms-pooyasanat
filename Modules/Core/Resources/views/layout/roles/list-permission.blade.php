@if(is_null($data))
<label for="permission"  class="form-control-label">{{__('cms.accessory')}}  </label>
                                    <select dir="rtl" class="form-control" id="permission" name="permission[]" required multiple>

@if(hasModule('Article'))
    @if(isset($permission_articles))
        <optgroup label="{{__('article::articles.collect')}}">
            @foreach($permission_articles as $permission_article)
                <option value="{{$permission_article->id}}">{{__('article::articles.permission.'.$permission_article->name)}}</option>
            @endforeach
        </optgroup>
    @endif
@endif



@if(hasModule('Information'))
    @if(isset($permission_informations))
        <optgroup label="{{__('information::informations.collect')}}">
            @foreach($permission_informations as $permission_information)
                <option value="{{$permission_information->id}}">{{__('information::informations.permission.'.$permission_information->name)}}</option>
            @endforeach
        </optgroup>
    @endif
@endif


@if(hasModule('Menu'))
    @if(isset($permission_menus))
        <optgroup label="{{__('menu::menus.collect')}}">
            @foreach($permission_menus as $permission_menu)
                <option value="{{$permission_menu->id}}">{{__('menu::menus.permission.'.$permission_menu->name)}}</option>
            @endforeach
        </optgroup>
    @endif
@endif



@if(hasModule('Customer'))
    @if(isset($permission_customers))
        <optgroup label="{{__('customer::customers.collect')}}">
            @foreach($permission_customers as $permission_customer)
                <option value="{{$permission_customer->id}}">{{__('customer::customers.permission.'.$permission_customer->name)}}</option>
            @endforeach
        </optgroup>
    @endif
@endif


@if(hasModule('Client'))
    @if(isset($permission_clients))
        <optgroup label="{{__('client::clients.collect')}}">
            @foreach($permission_clients as $permission_client)
                <option value="{{$permission_client->id}}">{{__('client::clients.permission.'.$permission_client->name)}}</option>
            @endforeach
        </optgroup>
    @endif
@endif

@if(hasModule('Member'))
    @if(isset($permission_members))
        <optgroup label="{{__('member::members.collect')}}">
            @foreach($permission_members as $permission_member)
                <option value="{{$permission_member->id}}">{{__('member::members.permission.'.$permission_member->name)}}</option>
            @endforeach
        </optgroup>
    @endif
@endif


@if(hasModule('User'))
    @if(isset($permission_users))
        <optgroup label="{{__('user::users.collect')}}">
            @foreach($permission_users as $permission_user)
                <option value="{{$permission_user->id}}">{{__('user::users.permission.'.$permission_user->name)}}</option>
            @endforeach
        </optgroup>
    @endif
@endif

@if(hasModule('Carousel'))
    @if(isset($permission_carousles))
        <optgroup label=" {{__('carousel::carousels.collect')}}">
            @foreach($permission_carousels as $permission_carousel)
                <option value="{{$permission_carousel->id}}">{{__('member::members.permission.'.$permission_carousel->name)}}</option>
            @endforeach
        </optgroup>
    @endif
@endif


@if(hasModule('Page'))
    @if(isset($permission_pages))
        <optgroup label=" {{__('page::pages.collect')}}">
            @foreach($permission_pages as $permission_page)
                <option value="{{$permission_page->id}}">{{__('page::pages.permission.'.$permission_page->name)}}</option>
            @endforeach
        </optgroup>
    @endif
@endif


@if(hasModule('Service'))
    @if(isset($permission_services))
        <optgroup label=" {{__('service::services.collect')}}">
            @foreach($permission_services as $permission_service)
                <option value="{{$permission_service->id}}">{{__('service::services.permission.'.$permission_service->name)}}</option>
            @endforeach
        </optgroup>
    @endif
    @if(isset($permission_advantages))
        <optgroup label=" {{__('advantage::advantages.collect')}}">
            @foreach($permission_advantages as $permission_advantage)
                <option value="{{$permission_advantage->id}}">{{__('service::advantages.permission.'.$permission_advantage->name)}}</option>
            @endforeach
        </optgroup>
    @endif
    @if(isset($permission_properties))
        <optgroup label=" {{__('property::properties.collect')}}">
            @foreach($permission_properties as $permission_property)
                <option value="{{$permission_property->id}}">{{__('service::properties.permission.'.$permission_property->name)}}</option>
            @endforeach
        </optgroup>
    @endif
@endif



@if(hasModule('Question'))
    @if(isset($permission_questions))
        <optgroup label=" {{__('questions::questions.collect')}}">
            @foreach($permission_questions as $permission_question)
                <option value="{{$permission_question->id}}">{{__('questions::questions.permission.'.$permission_question->name)}}</option>
            @endforeach
        </optgroup>
    @endif
@endif



@if(hasModule('Product'))
    @if(isset($permission_products))
        <optgroup label="{{__('product::products.collect')}}">
            @foreach($permission_products as $permission_product)
                <option value="{{$permission_product->id}}">{{__('product::products.permission.'.$permission_product->name)}}</option>
            @endforeach
        </optgroup>
    @endif
@endif


    @if(hasModule('Download'))
        @if(isset($permission_downloads))
            <optgroup label="{{__('download::downloads.collect')}}">
                @foreach($permission_downloads as $permission_download)
                    <option value="{{$permission_download->id}}">{{__('download::downloads.permission.'.$permission_download->name)}}</option>
                @endforeach
            </optgroup>
        @endif
    @endif



    @if(hasModule('Event'))
        @if(isset($permission_events))
            <optgroup label="{{__('event::events.collect')}}">
                @foreach($permission_events as $permission_event)
                    <option value="{{$permission_event->id}}">{{__('event::events.permission.'.$permission_event->name)}}</option>
                @endforeach
            </optgroup>
        @endif
    @endif



@if(hasModule('Portfolio'))
    @if(isset($permission_portfolios))
        <optgroup label="{{__('portfolio::portfolios.collect')}}">
            @foreach($permission_portfolios as $permission_portfolio)
                <option value="{{$permission_portfolio->id}}">{{__('portfolio::portfolios.permission.'.$permission_portfolio->name)}}</option>
            @endforeach
        </optgroup>
    @endif
@endif


@if(hasModule('Plan'))
    @if(isset($permission_palnes))
        <optgroup label=" {{__('plan::plans.collect')}}">
            @foreach($permission_planes as $permission_plan)
                <option value="{{$permission_plan->id}}">{{__('plan::planes.permission.'.$permission_plan->name)}}</option>
            @endforeach
        </optgroup>
    @endif
@endif

    @if(hasModule('Brand'))
        @if(isset($permission_brands))
            <optgroup label=" {{__('brand::brands.collect')}}">
                @foreach($permission_brands as $permission_brand)
                    <option value="{{$permission_brand->id}}">{{__('brand::brands.permission.'.$permission_brand->name)}}</option>
                @endforeach
            </optgroup>
        @endif
    @endif


    @if(hasModule('Store'))
        @if(isset($permission_stores))
            <optgroup label=" {{__('store::stores.collect')}}">
                @foreach($permission_stores as $permission_store)
                    <option value="{{$permission_store->id}}">{{__('store::stores.permission.'.$permission_store->name)}}</option>
                @endforeach
            </optgroup>
        @endif
    @endif

    @if(hasModule('User'))
        @if(isset($permission_users))
            <optgroup label=" {{__('user::users.collect')}}">
                @foreach($permission_users as $permission_user)
                    <option value="{{$permission_user->id}}">{{__('user::users.permission.'.$permission_user->name)}}</option>
                @endforeach
            </optgroup>
        @endif
    @endif


@if(hasModule('Sms'))
    @if(isset($permission_smses))
        <optgroup label=" {{__('sms::smses.collect')}}">
            @foreach($permission_smses as $permission_sms)
                <option value="{{$permission_sms->id}}">{{__('sms::smses.permission.'.$permission_sms->name)}}</option>
            @endforeach
        </optgroup>
        @endif
        @endif

        </select>




































{{--Data is null    --}}




@else

    <label for="permission"  class="form-control-label">{{__('cms.accessory')}}  </label>
    <select dir="rtl" class="form-control" id="permission" name="permission[]"  required multiple>

        @if(hasModule('Article'))
            @if(isset($permission_articles))
                <optgroup label="{{__('article::articles.collect')}}">
                    @foreach($permission_articles as $permission_article)
                        <option value="{{$permission_article->id}}" {{in_array($permission_article->id,$rolePermissions) ? "selected": ""}}>{{__('article::articles.permission.'.$permission_article->name)}}</option>
                    @endforeach
                </optgroup>
            @endif
        @endif

        @if(hasModule('Information'))
            @if(isset($permission_informations))
                <optgroup label="{{__('information::informations.collect')}}">
                    @foreach($permission_informations as $permission_information)
                        <option value="{{$permission_information->id}}" {{in_array($permission_information->id,$rolePermissions) ? "selected": ""}}>{{__('information::informations.permission.'.$permission_information->name)}}</option>
                    @endforeach
                </optgroup>
            @endif
        @endif


        @if(hasModule('Menu'))
            @if(isset($permission_menus))
                <optgroup label="{{__('menu::menus.collect')}}">
                    @foreach($permission_menus as $permission_menu)
                        <option value="{{$permission_menu->id}}" {{in_array($permission_menu->id,$rolePermissions) ? "selected": ""}}>{{__('menu::menus.permission.'.$permission_menu->name)}}</option>
                    @endforeach
                </optgroup>
            @endif
        @endif



        @if(hasModule('Customer'))
            @if(isset($permission_customers))
                <optgroup label="{{__('customer::customers.collect')}}">
                    @foreach($permission_customers as $permission_customer)
                        <option value="{{$permission_customer->id}}" {{in_array($permission_customer->id,$rolePermissions) ? "selected": ""}}>{{__('customer::customers.permission.'.$permission_customer->name)}}</option>
                    @endforeach
                </optgroup>
            @endif
        @endif


        @if(hasModule('Client'))
            @if(isset($permission_clients))
                <optgroup label="{{__('client::clients.collect')}}">
                    @foreach($permission_clients as $permission_client)
                        <option value="{{$permission_client->id}}" {{in_array($permission_client->id,$rolePermissions) ? "selected": ""}}>{{__('client::clients.permission.'.$permission_client->name)}}</option>
                    @endforeach
                </optgroup>
            @endif
        @endif

        @if(hasModule('Member'))
            @if(isset($permission_members))
                <optgroup label="{{__('member::members.collect')}}">
                    @foreach($permission_members as $permission_member)
                        <option value="{{$permission_member->id}}" {{in_array($permission_member->id,$rolePermissions) ? "selected": ""}}>{{__('member::members.permission.'.$permission_member->name)}}</option>
                    @endforeach
                </optgroup>
            @endif
        @endif


        @if(hasModule('User'))
            @if(isset($permission_users))
                <optgroup label="{{__('user::users.collect')}}">
                    @foreach($permission_users as $permission_user)
                        <option value="{{$permission_user->id}}" {{in_array($permission_user->id,$rolePermissions) ? "selected": ""}}>{{__('user::users.permission.'.$permission_user->name)}}</option>
                    @endforeach
                </optgroup>
            @endif
        @endif

        @if(hasModule('Carousel'))
            @if(isset($permission_carousles))
                <optgroup label=" {{__('carousel::carousels.collect')}}">
                    @foreach($permission_carousels as $permission_carousel)
                        <option value="{{$permission_carousel->id}}" {{in_array($permission_carousel->id,$rolePermissions) ? "selected": ""}}>{{__('member::members.permission.'.$permission_carousel->name)}}</option>
                    @endforeach
                </optgroup>
            @endif
        @endif


        @if(hasModule('Page'))
            @if(isset($permission_pages))
                <optgroup label=" {{__('page::pages.collect')}}">
                    @foreach($permission_pages as $permission_page)
                        <option value="{{$permission_page->id}}" {{in_array($permission_page->id,$rolePermissions) ? "selected": ""}}>{{__('page::pages.permission.'.$permission_page->name)}}</option>
                    @endforeach
                </optgroup>
            @endif
        @endif


        @if(hasModule('Service'))
            @if(isset($permission_services))
                <optgroup label=" {{__('service::services.collect')}}">
                    @foreach($permission_services as $permission_service)
                        <option value="{{$permission_service->id}}" {{in_array($permission_service->id,$rolePermissions) ? "selected": ""}}>{{__('service::services.permission.'.$permission_service->name)}}</option>
                    @endforeach
                </optgroup>
            @endif
            @if(isset($permission_advantages))
                <optgroup label=" {{__('advantage::advantages.collect')}}">
                    @foreach($permission_advantages as $permission_advantage)
                        <option value="{{$permission_advantage->id}}" {{in_array($permission_advantage->id,$rolePermissions) ? "selected": ""}}>{{__('service::advantages.permission.'.$permission_advantage->name)}}</option>
                    @endforeach
                </optgroup>
            @endif
            @if(isset($permission_properties))
                <optgroup label=" {{__('property::properties.collect')}}">
                    @foreach($permission_properties as $permission_property)
                        <option value="{{$permission_property->id}}" {{in_array($permission_property->id,$rolePermissions) ? "selected": ""}}>{{__('service::properties.permission.'.$permission_property->name)}}</option>
                    @endforeach
                </optgroup>
            @endif
        @endif


        @if(hasModule('Question'))
            @if(isset($permission_questions))
                <optgroup label=" {{__('questions::questions.collect')}}">
                    @foreach($permission_questions as $permission_question)
                        <option value="{{$permission_question->id}}" {{in_array($permission_question->id,$rolePermissions) ? "selected": ""}}>{{__('questions::questions.permission.'.$permission_question->name)}}</option>
                    @endforeach
                </optgroup>
            @endif
        @endif



        @if(hasModule('Product'))
            @if(isset($permission_products))
                <optgroup label="{{__('product::products.collect')}}">
                    @foreach($permission_products as $permission_product)
                        <option value="{{$permission_product->id}}" {{in_array($permission_product->id,$rolePermissions) ? "selected": ""}}>{{__('product::products.permission.'.$permission_product->name)}}</option>
                    @endforeach
                </optgroup>
            @endif
        @endif


            @if(hasModule('Download'))
                @if(isset($permission_downloads))
                    <optgroup label="{{__('download::downloads.collect')}}">
                        @foreach($permission_downloads as $permission_download)
                            <option value="{{$permission_download->id}}" {{in_array($permission_download->id,$rolePermissions) ? "selected": ""}}>{{__('download::downloads.permission.'.$permission_download->name)}}</option>
                        @endforeach
                    </optgroup>
                @endif
            @endif



            @if(hasModule('Event'))
                @if(isset($permission_events))
                    <optgroup label="{{__('event::events.collect')}}">
                        @foreach($permission_events as $permission_event)
                            <option value="{{$permission_event->id}}" {{in_array($permission_event->id,$rolePermissions) ? "selected": ""}}>{{__('event::events.permission.'.$permission_event->name)}}</option>
                        @endforeach
                    </optgroup>
                @endif
            @endif


        @if(hasModule('Portfolio'))
            @if(isset($permission_portfolios))
                <optgroup label="{{__('portfolio:portfolios.collect')}}">
                    @foreach($permission_portfolios as $permission_portfolio)
                        <option value="{{$permission_portfolio->id}}"  {{in_array($permission_portfolio->id,$rolePermissions) ? "selected": ""}}>{{__('portfolio::portfolios.permission.'.$permission_portfolio->name)}}</option>
                    @endforeach
                </optgroup>
            @endif
        @endif


        @if(hasModule('Plan'))
            @if(isset($permission_palnes))
                <optgroup label=" {{__('plan::plans.collect')}}">
                    @foreach($permission_planes as $permission_plan)
                        <option value="{{$permission_plan->id}}"{{in_array($permission_plan->id,$rolePermissions) ? "selected": ""}}>{{__('plan::planes.permission.'.$permission_plan->name)}}</option>
                    @endforeach
                </optgroup>
            @endif
        @endif

            @if(hasModule('Brand'))
                @if(isset($permission_brands))
                    <optgroup label=" {{__('brand::brands.collect')}}">
                        @foreach($permission_brands as $permission_brand)
                            <option value="{{$permission_brand->id}}"{{in_array($permission_brand->id,$rolePermissions) ? "selected": ""}}>{{__('brand::brands.permission.'.$permission_brand->name)}}</option>
                        @endforeach
                    </optgroup>
                @endif
            @endif


            @if(hasModule('Store'))
                @if(isset($permission_stores))
                    <optgroup label=" {{__('store::stores.collect')}}">
                        @foreach($permission_stores as $permission_store)
                            <option value="{{$permission_store->id}}"{{in_array($permission_store->id,$rolePermissions) ? "selected": ""}}>{{__('store::stores.permission.'.$permission_store->name)}}</option>
                        @endforeach
                    </optgroup>
                @endif
            @endif

            @if(hasModule('User'))
                @if(isset($permission_users))
                    <optgroup label=" {{__('user::users.collect')}}">
                        @foreach($permission_users as $permission_user)
                            <option value="{{$permission_user->id}}"{{in_array($permission_user->id,$rolePermissions) ? "selected": ""}}>{{__('user::users.permission.'.$permission_user->name)}}</option>
                        @endforeach
                    </optgroup>
                @endif
            @endif



        @if(hasModule('Sms'))
            @if(isset($permission_smses))
                <optgroup label=" {{__('sms::smses.collect')}}">
                    @foreach($permission_smses as $permission_sms)
                        <option value="{{$permission_sms->id}}" {{in_array($permission_sms->id,$rolePermissions) ? "selected": ""}}>{{__('sms::smses.permission.'.$permission_sms->name)}}</option>
                    @endforeach
                </optgroup>
            @endif
        @endif







    </select>


@endif
