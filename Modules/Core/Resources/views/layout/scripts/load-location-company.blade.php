<script>


    loadCountries();

    $('#company_country').change(function(){

        loadStates($(this));


    });

    $('#company_state').change(function(){

        loadCities($(this));

    });

    $('#company_city').change(function(){

        loadAreas($(this));

    });




    function loadCountries(){
        $.ajax({
            type:'GET',
            url:'/ajax/load/countries',
            success:function(response) {
                console.log(response)
                $.each(response.data, function(val, text) {
                    $('#company_country').append( $('<option></option>').val(text.id).html(text.name) )
                });

            },
            error:function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }

    function loadStates(item){
        $.ajax({
            type:'GET',
            url:'/ajax/load/country/states/'+item.val(),
            success:function(response) {
                console.log(response)
                $('#company_city').empty()
                $('#company_area').empty()
                $.each(response.data, function(val, text) {
                    $('#company_state')
                        .append( $('<option></option>').val(text.id).html(text.name) )
                });

            },
            error:function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }


    function loadCities(item){
        $.ajax({
            type:'GET',
            url:'/ajax/cities/'+item.val(),
            success:function(response) {
                console.log(response)
                $('#company__city').empty()
                $.each(response.data, function(val, text) {
                    $('#company__city')
                        .append( $('<option></option>').val(text.id).html(text.name) )
                });

            },
            error:function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }

    function loadAreas(item){
        $.ajax({
            type:'GET',
            url:'/ajax/areas/'+item.val(),
            success:function(response) {
                console.log(response)
                $('#company_area').empty()
                $.each(response.data, function(val, text) {
                    $('#company_area')
                        .append( $('<option></option>').val(text.id).html(text.name) )


                });

            },
            error:function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }


</script>
