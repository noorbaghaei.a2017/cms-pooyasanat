<?php

namespace Modules\Core\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;

class Award extends Model
{
    use Sluggable,TimeAttribute;

    protected $table="awards";

    protected $fillable = ['parent','level','icon','slug','excerpt','token','order','user','title','symbol'];

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
