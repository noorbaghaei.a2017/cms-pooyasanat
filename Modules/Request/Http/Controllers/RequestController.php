<?php

namespace Modules\Request\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Mockery\Exception;
use Modules\Request\Entities\listRequest;

class RequestController extends Controller
{
    protected $entity;

    public function __construct()
    {
        $this->entity=new \Modules\Request\Entities\Request();

        $this->middleware('permission:request-list')->only('index');
        $this->middleware('permission:request-create')->only(['create','store']);
        $this->middleware('permission:request-edit' )->only(['edit','update']);
        $this->middleware('permission:request-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('request::requests.index',compact('items'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }

    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->email)
            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('request::requests.index',compact('items'));
            }
            $items=$this->entity
                ->where("email",'LIKE','%'.trim($request->email).'%')
                ->paginate(config('cms.paginate'));
            return view('request::requests.index',compact('items','request'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('request::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {


            $this->entity->email=$request->input('email');
            $this->entity->name=$request->input('name');
            $this->entity->phone=$request->input('phone');
            $this->entity->country='german';
            $this->entity->gender=$request->input('gender');
            $this->entity->loss=$request->input('loss');
            $this->entity->color=$request->input('color');
            $this->entity->fall_time=$request->input('fall_time');
            $this->entity->transplantation=$request->input('transplantation');
            $this->entity->feeling=$request->input('feeling');
            $this->entity->execution_time=$request->input('execution_time');
            $this->entity->List_requests=listRequest::whereName('hair-transplantation')->firstOrFail()->id;
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            if(!$saved){
                return redirect()->back()->with('error',__('menu::menus.error'));
            }else{
                return redirect(route("front.website"))->with('message',__('menu::menus.store'));
            }



        }catch (Exception $exception){

            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('request::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('request::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
