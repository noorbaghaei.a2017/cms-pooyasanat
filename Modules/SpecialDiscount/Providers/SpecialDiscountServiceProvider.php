<?php

namespace Modules\SpecialDiscount\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class SpecialDiscountServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('SpecialDiscount', 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('SpecialDiscount', 'Config/config.php') => config_path('specialdiscount.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('SpecialDiscount', 'Config/config.php'), 'specialdiscount'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/specialdiscount');

        $sourcePath = module_path('SpecialDiscount', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/specialdiscount';
        }, \Config::get('view.paths')), [$sourcePath]), 'specialdiscount');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/specialdiscount');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'specialdiscount');
        } else {
            $this->loadTranslationsFrom(module_path('SpecialDiscount', 'Resources/lang'), 'specialdiscount');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('SpecialDiscount', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
