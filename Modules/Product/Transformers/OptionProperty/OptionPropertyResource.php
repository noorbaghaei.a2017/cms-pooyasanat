<?php

namespace Modules\Product\Transformers\OptionProperty;

use Illuminate\Http\Resources\Json\Resource;

class OptionPropertyResource extends Resource
{
    public $preserveKeys = true;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title'=>$this->title ,
            'slug'=>$this->slug ,
            'order'=>$this->order ,
            'update_date'=>$this->AgoTimeUpdate,
            'create_date'=>$this->TimeCreate,
            'token'=>$this->token,

        ];
    }
}
