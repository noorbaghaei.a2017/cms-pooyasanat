<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Http\Controllers\HasOption;
use Modules\Product\Entities\OptionProperty;
use Modules\Product\Entities\Repository\OptionProperty\OptionPropertyRepositoryInterface;
use Modules\Product\Transformers\OptionProperty\OptionPropertyCollection;

class OptionPropertyController extends Controller
{
    use HasOption;

    protected $entity;
    protected $class;
    private $repository;

//option

    protected $route_options_index='product::properties.options.index';
    protected $route_options_create='product::properties.options.create';
    protected $route_options_edit='product::properties.options.edit';
    protected $route_options='properties.options';

//notification

    protected $notification_store='product::properties.store';
    protected $notification_update='product::properties.update';
    protected $notification_delete='product::properties.delete';
    protected $notification_error='product::properties.error';


    public function __construct(OptionPropertyRepositoryInterface $repository)
    {
        $this->entity=new OptionProperty();

        $this->class=OptionProperty::class;

        $this->repository=$repository;

        $this->middleware('permission:product-list')->only('index');
        $this->middleware('permission:product-create')->only(['create','store']);
        $this->middleware('permission:product-edit' )->only(['edit','update']);
        $this->middleware('permission:product-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->repository->getAll();

            $result = new OptionPropertyCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    public function search(Request $request)
    {
        try {

            if(
                !isset($request->title) &&
                !isset($request->code)
            ){
                $items=$this->repository->getAll();

                $result = new OptionPropertyCollection($items);

                $data= collect($result->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }

            $items=$this->entity
                ->where("title",trim($request->title))
                ->orwhere("code",trim($request->code))
                ->paginate(config('cms.paginate'));

            $result = new OptionPropertyCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('product::properties.create');
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->icon=$request->input('icon');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->text=$request->input('text');
            $this->entity->order=orderInfo($request->input('order'));
            $this->entity->token=tokenGenerate();


            $saved=$this->entity->save();


            if(!$saved){
                return redirect()->back()->with('error',__('product::properties.error'));
            }else{
                return redirect(route("properties.index"))->with('message',__('product::properties.store'));
            }



        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            return view('product::properties.edit',compact('item'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $token)
    {
        try {

            $this->entity=$this->entity->whereToken($token)->firstOrFail();

            $updated=$this->entity->update([
                "user"=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "excerpt"=>$request->input('excerpt'),
                "icon"=>$request->input('icon'),
                "text"=>$request->input('text'),
                "order"=>orderInfo($request->input('order'))
            ]);

            $this->entity->replicate();


            if(!$updated){
                return redirect()->back()->with('error',__('product::properties.error'));
            }else{
                return redirect(route("properties.index"))->with('message',__('product::properties.update'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
