<?php
return [
    "text-create"=>"you can create your property",
    "text-edit"=>"you can edit your property",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"properties list",
    "singular"=>"property",
    "collect"=>"properties",
    "permission"=>[
        "product-full-access"=>"properties full access",
        "product-list"=>"properties list",
        "product-delete"=>"property delete",
        "product-create"=>"property create",
        "product-edit"=>"edit property",
    ]
];
