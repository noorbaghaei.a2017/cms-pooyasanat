@include('core::layout.modules.option.edit',[

    'title'=>__('core::options.create'),
    'item'=>$item,
    'parent'=>'product',
    'model'=>'property',
    'directory'=>'properties',
    'collect'=>__('core::options.collect'),
    'singular'=>__('core::options.singular'),
   'update_route'=>['name'=>'property.option.update','param'=>'option'],

])








