<?php


namespace Modules\Product\Entities\Repository\ProductOption;



use Modules\Product\Entities\ProductOption;

class ProductOptionRepository implements ProductOptionRepositoryInterface
{

    public function getAll()
    {
        return ProductOption::latest()->get();
    }
}
