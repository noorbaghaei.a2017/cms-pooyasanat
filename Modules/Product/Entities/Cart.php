<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = ['first_name','last_name','mobile','email','address','status','excerpt','token','client','postal_code'];

    public function items(){
        return $this->hasMany(CartList::class,'cart','id')->with('product_item');
    }

}
