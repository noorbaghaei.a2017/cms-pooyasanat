<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cart')->unsigned();;
            $table->foreign('cart')->references('id')->on('carts')->onDelete('cascade');
            $table->bigInteger('product')->unsigned();;
            $table->foreign('product')->references('id')->on('products')->onDelete('cascade');
            $table->string('count')->default(1);
            $table->string('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_lists');
    }
}
