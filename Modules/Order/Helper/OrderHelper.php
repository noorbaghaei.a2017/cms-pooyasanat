<?php


namespace Modules\Order\Helper;


class OrderHelper
{
    public static function status($status){
        switch ($status){
            case 0 :
                return '<span class="alert-danger">ناموفق</span>';
                break;
            case 1 :
                return '<span class="alert-success">موفق</span>';
                break;
            default:
                return '<span class="alert-danger">خطای سیستمی</span>';
                break;

        }
    }
    public static function statusValue($status){
        switch ($status){
            case 0 :
                return 'ناموفق';
                break;
            case 1 :
                return 'موفق';
                break;
            default:
                return 'خطای سیستمی';
                break;

        }
    }
}
