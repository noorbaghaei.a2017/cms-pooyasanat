<?php

namespace Modules\Order\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Order\Entities\Order;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Order::class)->delete();

        Permission::create(['name'=>'order-list','model'=>Order::class,'created_at'=>now()]);
        Permission::create(['name'=>'order-create','model'=>Order::class,'created_at'=>now()]);
        Permission::create(['name'=>'order-edit','model'=>Order::class,'created_at'=>now()]);
        Permission::create(['name'=>'order-delete','model'=>Order::class,'created_at'=>now()]);

    }
}
