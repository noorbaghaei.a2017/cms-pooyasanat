<?php

namespace Modules\Customer\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Customer\Entities\Customer;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Customer::class)->delete();

        Permission::create(['name'=>'customer-list','model'=>Customer::class,'created_at'=>now()]);
        Permission::create(['name'=>'customer-create','model'=>Customer::class,'created_at'=>now()]);
        Permission::create(['name'=>'customer-edit','model'=>Customer::class,'created_at'=>now()]);
        Permission::create(['name'=>'customer-delete','model'=>Customer::class,'created_at'=>now()]);
    }
}
