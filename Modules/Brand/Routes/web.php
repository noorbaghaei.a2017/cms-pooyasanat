<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'),"middleware"=>["auth:web"]],function() {
    Route::resource('/brands', 'BrandController');

    Route::group(["prefix"=>'search', "middleware" => ["auth:web"]], function () {
        Route::post('/brands', 'BrandController@search')->name('search.brand');
    });

});
