<?php
return [
    "text-create"=>"you can create your ad",
    "text-edit"=>"you can edit your ad",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"ads list",
    "error"=>"error",
    "singular"=>"ad",
    "collect"=>"ads",
    "permission"=>[
        "ad-full-access"=>"ads full access",
        "ad-list"=>"ads list",
        "ad-delete"=>"ad delete",
        "ad-create"=>"ad create",
        "ad-edit"=>"edit ad",
    ]


];
