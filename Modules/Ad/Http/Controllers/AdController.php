<?php

namespace Modules\Ad\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;
use Modules\Ad\Entities\Ad;
use Modules\Ad\Entities\Repository\AdRepositoryInterface;
use Modules\Ad\Http\Requests\AdRequest;
use Modules\Ad\Transformers\AdCollection;

class AdController extends Controller
{

    protected $entity;
    protected $class;
    private $repository;

    public function __construct(AdRepositoryInterface $repository)
    {
        $this->entity=new Ad();
        $this->class=Ad::class;
        $this->repository=$repository;
        $this->middleware('permission:ad-list');
        $this->middleware('permission:ad-create')->only(['create','store']);
        $this->middleware('permission:ad-edit' )->only(['edit','update']);
        $this->middleware('permission:ad-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->repository->getAll();

            $result = new AdCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('ad::ads.create');
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
            !isset($request->title) &&
            !isset($request->slug) &&
            !isset($request->order)
            ){
                $items=$this->repository->getAll();

                $result = new AdCollection($items);

                $data= collect($result->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }
            $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->where("slug",'LIKE','%'.trim($request->slug).'%')
                ->where("order",'LIKE','%'.trim($request->order).'%')
                ->paginate(config('cms.paginate'));

            $result = new AdCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(AdRequest $request)
    {
        try {
            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->excerpt=$request->input('excerpt');
            $this->entity->order=$request->input('order');
            $this->entity->start_at=convertJalali($request->date_start,$request->time_start);
            $this->entity->end_at=convertJalali($request->date_end,$request->time_end);
            $this->entity->text=$request->input('text');
            $this->entity->href=$request->input('href');
            $this->entity->status=$request->input('status');
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            $this->entity->analyzer()->create();

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$saved){
                return redirect()->back()->with('error',__('ad::ads.error'));
            }else{
                return redirect(route("ads.index"))->with('message',__('ad::ads.store'));
            }
        }catch (Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('ad::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            return view('ad::ads.edit',compact('item'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param AdRequest $request
     * @param $token
     * @return Response
     */
    public function update(AdRequest $request, $token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                'user'=>auth('web')->user()->id,
                "slug"=>null,
                "title"=>$request->input('title'),
                "start_at"=>convertJalali($request->input('date_start'),$request->input('time_start')),
                "end_at"=>convertJalali($request->input('date_end'),$request->input('time_end')),
                "excerpt"=>$request->input('excerpt'),
                "order"=>$request->input('order'),
                "href"=>$request->input('href'),
                "status"=>$request->input('status'),
                "text"=>$request->input('text'),
            ]);
            $this->entity->replicate();

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$updated){
                return redirect()->back()->with('error',__('ad::ads.error'));
            }else{
                return redirect(route("ads.index"))->with('message',__('ad::ads.update'));
            }


        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($token)
    {
        try {
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            if($this->entity->Hasmedia(config('cms.collection-image'))){
                destroyMedia($this->entity,config('cms.collection-image'));
            }
            $deleted=$this->entity->delete();

            if(!$deleted){
                return redirect()->back()->with('error',__('ad::ads.error'));
            }else{
                return redirect(route("ads.index"))->with('message',__('ad::ads.delete'));
            }

        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
}
