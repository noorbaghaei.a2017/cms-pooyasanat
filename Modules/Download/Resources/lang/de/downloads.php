<?php
return [
    "text-create"=>"you can create your download",
    "text-edit"=>"you can edit your download",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"downloads list",
    "error"=>"error",
    "singular"=>"download",
    "collect"=>"downloads",
    "permission"=>[
        "download-full-access"=>"download full access",
        "download-list"=>"downloads list",
        "download-delete"=>"download delete",
        "download-create"=>"download create",
        "download-edit"=>"edit download",
    ]


];
