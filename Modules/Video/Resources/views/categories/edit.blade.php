@include('core::layout.modules.category.edit',[

    'title'=>__('core::categories.create'),
    'item'=>$item,
    'parent'=>'video',
    'model'=>'video',
    'directory'=>'videos',
    'collect'=>__('core::categories.collect'),
    'singular'=>__('core::categories.singular'),
   'update_route'=>['name'=>'video.category.update','param'=>'category'],

])








