<?php

namespace Modules\Client\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddAdvertEmployer extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|max:30',
            'text'=>'required|max:100',
            'excerpt'=>'required|max:50',
            'image'=>'mimes:jpeg,png,jpg|max:2000',
            'kinds'=>'required',
            'skills'=>'required',
            'number_employees'=>'required|numeric',
            'category'=>'required',
            'salary'=>'required',
            'experience'=>'required',
            'positions'=>'required',
            'educations'=>'required',
            'guild'=>'required',
            'currency'=>'required',
            'gender'=>'required',
        ];

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
