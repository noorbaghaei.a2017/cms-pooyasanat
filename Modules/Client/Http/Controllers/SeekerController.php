<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Modules\Client\Entities\Client;
use Modules\Client\Entities\ClientRole;
use Modules\Client\Http\Requests\SeekerRequest;
use Modules\Core\Entities\Country;

class SeekerController extends Controller
{
    protected $entity;

    public function __construct()
    {
        $this->entity=new Client();

        $this->middleware('permission:seeker-list')->only('index');
        $this->middleware('permission:seeker-create')->only(['create','store']);
        $this->middleware('permission:seeker-edit' )->only(['edit','update']);
        $this->middleware('permission:seeker-delete')->only(['destroy']);
    }
    use  ResetsPasswords;

    public function resetPass($object,$pass){

        $this->resetPassword($object,$pass);
    }
    public function resetPasswordProfile($object,$current,$pass){

        if(Hash::check($current, $object->password)){

            $this->resetPassword($object,$pass);

            return $errorStatus=true;
        }
        else{
            return $errorStatus=false;
        }
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity
                ->with('myCode','seo')
                ->whereHas('role',function ($query){
                    $query->where('title', '=', 'seeker');
                })
                ->paginate(config('cms.paginate'));
            return view('client::seekers.index',compact('items'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('client::seekers.create');
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(SeekerRequest $request)
    {
        try {
            $this->entity->first_name=$request->input('firstname');
            $this->entity->last_name=$request->input('lastname');
            $this->entity->email=$request->input('email');
            $this->entity->mobile=$request->input('mobile');
            $this->entity->identity_card=$request->input('identity_card');
            $this->entity->password=Hash::make($request->input('password'));
            $this->entity->role=ClientRole::whereTitle('seeker')->firstOrFail()->id;
            $this->entity->country=$request->input('country');
            $this->entity->phone=$request->input('phone');
            $this->entity->is_active=2;
            $this->entity->postal_code=$request->input('postal_code');
            $this->entity->city_birthday=$request->input('city_birthday');
            $this->entity->address=$request->input('address');
            $this->entity->name=$request->input('name');
            $this->entity->token=tokenGenerate();

            $this->entity->save();


            $this->entity->analyzer()->create();

            $this->entity->myCode()->create([
                'code'=>generateCode()
            ]);

            $this->entity->info()->create([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'private'=>$request->access,
                'about'=>$request->about,
                'address'=>$request->info_address,
                'country'=>$request->info_country,
                'state'=>$request->info_state,
                'city'=>$request->info_city,
                'area'=>$request->info_area,
            ]);

            $this->entity->company()->create([
                'name'=>$request->company_name,
                'count_member'=>$request->count_member,
                'mobile'=>$request->company_mobile,
                'phone'=>$request->company_phone,
                'address'=>$request->company_address,
                'country'=>$request->company_country,
                'state'=>$request->company_state,
                'city'=>$request->company_city,
                'area'=>$request->company_area,
            ]);


            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            $this->entity->wallet()->create([
                'score'=>500
            ]);

            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            return redirect(route("seekers.index"))->with('message',__('client::seekers.store'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('client::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->with('info','company','seo')->first();
            return view('client::seekers.edit',compact('item'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(SeekerRequest $request, $token)
    {
        try {

            $this->entity=$this->entity->whereToken($token)->firstOrFail();

            $update=$this->entity->update([
                "first_name"=>$request->input('firstname'),
                "last_name"=>$request->input('lastname'),
                "mobile"=>$request->input('mobile'),
                "email"=>$request->input('email'),
                "identity_card"=>$request->input('identity_card'),
                "postal_code"=>$request->input('postal_code'),
                "phone"=>$request->input('phone'),
                "is_active"=>2,
                "address"=>$request->input('address'),
                "city_birthday"=>$request->input('city_birthday'),
                "username"=>$request->input('username'),
                "country"=>$request->input('country'),
                "name"=>$request->input('name'),
                "two_step"=>$request->input('two_step'),

            ]);

            $this->entity->info()->update([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'private'=>$request->access,
                'about'=>$request->about,
                'address'=>$request->info_address,
                'country'=>$request->info_country,
                'state'=>$request->info_state,
                'city'=>$request->info_city,
                'area'=>$request->info_area,
            ]);

            $this->entity->company()->update([
                'name'=>$request->company_name,
                'count_member'=>$request->count_member,
                'mobile'=>$request->company_mobile,
                'phone'=>$request->company_phone,
                'address'=>$request->company_address,
                'country'=>$request->company_country,
                'state'=>$request->company_state,
                'city'=>$request->company_city,
                'area'=>$request->company_area,
            ]);


            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!empty(trim($request->input('current_password'))) && !empty(trim($request->input('new_password')))  ){


                if(Hash::check($request->input('current_password'), $this->entity->password)){
                    $result=$this->entity->update([
                        'password'=>Hash::make($request->input('new_password'))
                    ]);
                    if(!$result){
                        return redirect()->back()->with('error',__('client::seekers.error-password'));
                    }

                }
                else{
                    return redirect()->back()->with('error',__('client::seekers.error-password'));


                }

            }

            if(!$update){
                return redirect()->back()->with('error',__('client::clients.error'));
            }else{
                return redirect(route("seekers.index"))->with('message',__('client::clients.update'));

            }

        }catch (\Exception $exception){

            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
