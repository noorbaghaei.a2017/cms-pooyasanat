<?php

namespace Modules\Advertising\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdvertisingRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'member_count'=>'required',
            'number_limit'=>'required',
            'time_limit'=>'required',
            'title'=>'required|string',
            'text'=>'required|string',
            'excerpt'=>'required|string',
            'image'=>'mimes:jpeg,png,jpg|max:2000',
            'kinds'=>'required',
            'skills'=>'required',
            'number_employees'=>'required|numeric',
            'category'=>'required',
            'salary'=>'required',
            'employer'=>'required',
            'experience'=>'required',
            'positions'=>'required',
            'educations'=>'required',
            'guild'=>'required',
            'currency'=>'required',
            'gender'=>'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
