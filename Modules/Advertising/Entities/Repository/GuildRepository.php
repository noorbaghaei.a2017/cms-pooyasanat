<?php


namespace Modules\Advertising\Entities\Repository;


use Modules\Advertising\Entities\Guild;

class GuildRepository implements GuildRepositoryInterface
{

    public function getAll()
    {
       return Guild::latest()->get();
    }
}
