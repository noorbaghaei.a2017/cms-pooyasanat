<?php


namespace Modules\Advertising\Entities\Repository;


use Modules\Advertising\Entities\Period;

class PeriodRepository implements PeriodRepositoryInterface
{

    public function getAll()
    {
       return Period::latest()->get();
    }
}
