<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Ad\Entities\Repository\AdRepository;
use Modules\Ad\Entities\Repository\AdRepositoryInterface;
use Modules\Advertising\Entities\Repository\AdvertisingRepository;
use Modules\Advertising\Entities\Repository\AdvertisingRepositoryInterface;
use Modules\Advertising\Entities\Repository\EducationRepository;
use Modules\Advertising\Entities\Repository\EducationRepositoryInterface;
use Modules\Advertising\Entities\Repository\ExperienceRepository;
use Modules\Advertising\Entities\Repository\ExperienceRepositoryInterface;
use Modules\Advertising\Entities\Repository\GuildRepository;
use Modules\Advertising\Entities\Repository\GuildRepositoryInterface;
use Modules\Advertising\Entities\Repository\PeriodRepository;
use Modules\Advertising\Entities\Repository\PeriodRepositoryInterface;
use Modules\Advertising\Entities\Repository\PositionRepository;
use Modules\Advertising\Entities\Repository\PositionRepositoryInterface;
use Modules\Advertising\Entities\Repository\SalaryRepository;
use Modules\Advertising\Entities\Repository\SalaryRepositoryInterface;
use Modules\Advertising\Entities\Repository\SkillRepository;
use Modules\Advertising\Entities\Repository\SkillRepositoryInterface;
use Modules\Advertising\Entities\Repository\TypeAdvertisingRepository;
use Modules\Advertising\Entities\Repository\TypeAdvertisingRepositoryInterface;
use Modules\Article\Entities\Repository\ArticleRepository;
use Modules\Article\Entities\Repository\ArticleRepositoryInterface;
use Modules\Brand\Entities\Repository\BrandRepository;
use Modules\Brand\Entities\Repository\BrandRepositoryInterface;
use Modules\Carousel\Entities\Repository\CarouselRepository;
use Modules\Carousel\Entities\Repository\CarouselRepositoryInterface;
use Modules\Core\Entities\Repository\AwardRepository;
use Modules\Core\Entities\Repository\AwardRepositoryInterface;
use Modules\Core\Entities\Repository\CurrencyRepository;
use Modules\Core\Entities\Repository\CurrencyRepositoryInterface;
use Modules\Customer\Entities\Repository\CustomerRepository;
use Modules\Customer\Entities\Repository\CustomerRepositoryInterface;
use Modules\Download\Entities\Repository\DownloadRepository;
use Modules\Download\Entities\Repository\DownloadRepositoryInterface;
use Modules\Educational\Entities\Repository\ClassRoomRepository;
use Modules\Educational\Entities\Repository\ClassRoomRepositoryInterface;
use Modules\Educational\Entities\Repository\RaceRepository;
use Modules\Educational\Entities\Repository\RaceRepositoryInterface;
use Modules\Event\Entities\Repository\EventRepository;
use Modules\Event\Entities\Repository\EventRepositoryInterface;
use Modules\Information\Entities\Repository\InformationRepository;
use Modules\Information\Entities\Repository\InformationRepositoryInterface;
use Modules\Menu\Entities\Repository\MenuRepository;
use Modules\Menu\Entities\Repository\MenuRepositoryInterface;
use Modules\Order\Entities\Repository\OrderRepository;
use Modules\Order\Entities\Repository\OrderRepositoryInterface;
use Modules\Payment\Http\Controllers\Geteway\Melat;
use Modules\Payment\Http\Controllers\Geteway\ZarinPal;
use Modules\Plan\Entities\Repository\PlanRepository;
use Modules\Plan\Entities\Repository\PlanRepositoryInterface;
use Modules\Portfolio\Entities\Repository\PortfolioRepository;
use Modules\Portfolio\Entities\Repository\PortfolioRepositoryInterface;
use Modules\Product\Entities\Repository\Product\ProductRepository;
use Modules\Product\Entities\Repository\Product\ProductRepositoryInterface;
use Modules\Product\Entities\Repository\OptionAttribute\OptionAttributeRepository;
use Modules\Product\Entities\Repository\OptionAttribute\OptionAttributeRepositoryInterface;
use Modules\Product\Entities\Repository\OptionProperty\OptionPropertyRepository;
use Modules\Product\Entities\Repository\OptionProperty\OptionPropertyRepositoryInterface;
use Modules\Service\Entities\Repository\ServiceRepository;
use Modules\Service\Entities\Repository\ServiceRepositoryInterface;
use Modules\Sms\Http\Controllers\Gateway\Farazsms;
use Modules\Sms\Http\Controllers\Gateway\Idepardazan;
use Modules\Sms\Http\Controllers\Gateway\Kavenegar;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $this->app->bind(MenuRepositoryInterface::class, MenuRepository::class);
        $this->app->bind(InformationRepositoryInterface::class, InformationRepository::class);
        $this->app->bind(ArticleRepositoryInterface::class, ArticleRepository::class);
        $this->app->bind(BrandRepositoryInterface::class, BrandRepository::class);
        $this->app->bind(AdvertisingRepositoryInterface::class, AdvertisingRepository::class);
        $this->app->bind(ClassRoomRepositoryInterface::class, ClassRoomRepository::class);
        $this->app->bind(RaceRepositoryInterface::class, RaceRepository::class);
        $this->app->bind(EventRepositoryInterface::class, EventRepository::class);
        $this->app->bind(ProductRepositoryInterface::class, ProductRepository::class);
        $this->app->bind(DownloadRepositoryInterface::class, DownloadRepository::class);
        $this->app->bind(AdRepositoryInterface::class, AdRepository::class);
        $this->app->bind(CarouselRepositoryInterface::class, CarouselRepository::class);
        $this->app->bind(ServiceRepositoryInterface::class, ServiceRepository::class);
        $this->app->bind(CustomerRepositoryInterface::class, CustomerRepository::class);
        $this->app->bind(PortfolioRepositoryInterface::class, PortfolioRepository::class);
        $this->app->bind(PlanRepositoryInterface::class, PlanRepository::class);
        $this->app->bind(OrderRepositoryInterface::class, OrderRepository::class);
        $this->app->bind(AwardRepositoryInterface::class, AwardRepository::class);
        $this->app->bind(CurrencyRepositoryInterface::class, CurrencyRepository::class);
        $this->app->bind(EducationRepositoryInterface::class, EducationRepository::class);
        $this->app->bind(GuildRepositoryInterface::class, GuildRepository::class);
        $this->app->bind(PeriodRepositoryInterface::class, PeriodRepository::class);
        $this->app->bind(PositionRepositoryInterface::class, PositionRepository::class);
        $this->app->bind(SalaryRepositoryInterface::class, SalaryRepository::class);
        $this->app->bind(SkillRepositoryInterface::class, SkillRepository::class);
        $this->app->bind(TypeAdvertisingRepositoryInterface::class, TypeAdvertisingRepository::class);
        $this->app->bind(ExperienceRepositoryInterface::class, ExperienceRepository::class);
        $this->app->bind(OptionAttributeRepositoryInterface::class, OptionAttributeRepository::class);
        $this->app->bind(OptionPropertyRepositoryInterface::class, OptionPropertyRepository::class);

    }
}
