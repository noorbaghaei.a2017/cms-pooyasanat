<?php

use Illuminate\Http\Request;
use Illuminate\Support\Str;

function tokenGenerate(){
    return Str::random(35);
}

function multiRouteKey(){
    return "token";
}

function orderInfo($order){
    return is_null(empty($order)) ? 1 : $order;
}

