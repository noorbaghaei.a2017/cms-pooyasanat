<div class="menu_btn_box header_btn jb_cover">
    <ul>

        @if(!auth('client')->check())
            <li>
                <a href="{{route('client.dashboard')}}"><i class="flaticon-login"></i> ورود</a>
            </li>
        @else

            <li>
                <a class="client" href="{{route('client.dashboard')}}"> <img src="{{asset('template/images/amin-nourbaghaei.jpeg')}}" alt="img" width="70"></a>
            </li>
        @endif
    </ul>
</div>
