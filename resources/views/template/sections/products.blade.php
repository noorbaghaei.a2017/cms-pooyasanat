@if(\Modules\Core\Helper\CoreHelper::hasProduct())
    <!-- start product Area -->
    <section class="owl-carousel active-product-area section_gap">
        <!-- single product slide -->
        <div class="single-product-slider">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                        <div class="section-title">
                            <h1>آخرین محصولات</h1>
                        </div>
                    </div>
                </div>
                <div class="row" style="direction: rtl">
                    @foreach($new_products as $product)
                    <!-- single product -->
                    <div class="col-lg-3 col-md-6">
                        <div class="single-product text-right">
                            <a href="{{route('products.single',['product'=>$product->slug])}}">

                                @if(!$product->Hasmedia('images'))
                                    <img class="img-fluid" src="{{asset('img/no-img.gif')}}" alt="">
                                @else
                                    <img class="img-fluid" src="{{$product->getFirstMediaUrl('images')}}" alt="">

                                @endif
                            </a>
                            <div class="product-details" style="margin-top: 10px">
                                <a href="{{route('products.single',['product'=>$product->slug])}}">
                                    <h6>{{$product->title}}</h6>
                                </a>
                            </div>
                        </div>
                    </div>
                        @endforeach


                </div>
            </div>
        </div>
        <!-- single product slide -->
        <div class="single-product-slider">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                        <div class="section-title">
                            <h1>به زودی</h1>

                        </div>
                    </div>
                </div>
                <div class="row" style="direction: rtl">
                @foreach($coming_products as $product)
                    <!-- single product -->
                    <div class="col-lg-3 col-md-6">
                        <div class="single-product text-right">
                            <a href="{{route('products.single',['product'=>$product->slug])}}">

                                @if(!$product->Hasmedia('images'))
                                    <img class="img-fluid" src="{{asset('img/no-img.gif')}}" alt="">
                                @else
                                    <img class="img-fluid" src="{{$product->getFirstMediaUrl('images')}}" alt="">

                                @endif

                            </a>
                            <div class="product-details">
                                <a href="{{route('products.single',['product'=>$product->slug])}}">
                                    <h6>{{$product->title}}</h6>
                                </a>

                            </div>
                        </div>
                    </div>
                    @endforeach


                </div>
            </div>
        </div>
    </section>
    <!-- end product Area -->
@endif
