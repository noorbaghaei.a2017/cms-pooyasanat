<!-- pricing plan wrapper start-->
<div class="pricing_plan_wrapper jb_cover">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-md-12 col-sm-12">
                <div class="jb_heading_wraper">

                    <h3>پلن مورد نظر را انتخاب کنید</h3>

                    <p>لطفا قبل از خرید امکانات هر پلن را مطالعه کنید</p>
                </div>
            </div>
            @foreach($plans as $plan)
            <div class="col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="pricing_box_wrapper jb_cover">
                    <div class="main_pdet jb_cover">
                      <h1>{{$plan->title}} </h1>
                      <div>

                          <h2>
                              {{number_format($plan->price->amount)}}
                              <span class="dollarr"> {{\Modules\Core\Entities\Currency::find($plan->currency->currency)->symbol}} </span>
                               </h2>
                          <span class="monthly"> {{\Modules\Plan\Helper\PlanHelper::periodValue($plan->period)}}</span>
                      </div>
                  </div>
                    <ul class="pricing_list22">
                       <li>
                           {{$plan->number_limit}} شغل ویژه
                       </li>

                        <li>
                             نمایش آگهی برای {{$plan->time_limit}} روز
                        </li>
                        <li>
                            پشتیبانی 7 روز 24 ساعته
                        </li>
                        <li>
                            انتشار در اپلیکیشن کارنیک
                        </li>
                        <li>
                            انتشار در شبکه های اجتماعی
                        </li>

                        @if($plan->notification->email)
                            <li>
                                هشدار ایمیل
                            </li>
                        @elseif($plan->notification->sms)
                            <li>
                                هشدار پیام
                            </li>
                        @elseif($plan->notification->call)
                            <li>
                                هشدار تماس
                            </li>
                         @endif

                        @if(!is_null($plan->attributes))
                            @foreach(json_decode($plan->attributes,true) as $value)
                                <li>
                                    {{$value}}
                                </li>
                            @endforeach
                        @endif

                    </ul>
                    <a href="#" class="price_btn">انتخاب طرح</a>
                </div>

            </div>
            @endforeach

        </div>
    </div>
</div>
<!-- pricing plan wrapper end-->
