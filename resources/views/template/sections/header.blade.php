
<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Author Meta -->
    {!! SEO::generate() !!}

    @yield('seo')

    <!-- meta character set -->
    <meta charset="UTF-8">
    <!--
        CSS
        ============================================= -->
    <meta name="google-site-verification" content="{{env('GOOGLE_VERIFY')}}" />

    <link rel="stylesheet" href="{{asset('template/css/linearicons.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/nouislider.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/ion.rangeSlider.css')}}" />
    <link rel="stylesheet" href="{{asset('template/css/ion.rangeSlider.skinFlat.css')}}" />
    <link rel="stylesheet" href="{{asset('template/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/style.css')}}">

@yield('head')

<!--favicon-->
    @if(!$setting->Hasmedia('logo'))
        <link rel="shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon" href="{{asset('img/no-img.gif')}}">
    @else
        <link rel="shortcut icon" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon" href="{{$setting->getFirstMediaUrl('logo')}}">
    @endif
</head>

<body>

<!-- Start Header Area -->
<header class="header_area sticky-header">
   @include('template.sections.menu')
</header>
<!-- End Header Area -->
