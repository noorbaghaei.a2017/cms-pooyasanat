

<!-- job category wrapper start-->
<div class="jb_category_wrapper jb_cover">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-md-12 col-sm-12">
                <div class="jb_heading_wraper">

                    <h3>مرور مشاغل توسط دسته</h3>

                    <p>دارایی شرکت توسعه محصول بعدی شما</p>
                </div>
            </div>
            @foreach($category_advertisings as $category)

                <div class="col-lg-3 col-md-6 col-sm-12">

                    <div class="jb_browse_category jb_cover">
                        <a href="job_listing_list_left_filter.html">
                            <div class="hover-block"></div>

                            <i class="flaticon-code"></i>
                            <h3>{{$category->symbol}}</h3>
                            <p>{{count($category->advertisings)}}</p>
                        </a>
                    </div>
                </div>

            @endforeach
            <div class="header_btn search_btn load_btn jb_cover">

                <a href="#">مشاغل بیشتر</a>

            </div>
        </div>
    </div>
</div>
<!-- job category wrapper end-->
