@if(\Modules\Core\Helper\CoreHelper::hasCustomer())
<!-- Start brand Area -->
<section class="brand-area section_gap">
    <div class="container">
        <div class="section-title text-center">
            <h2>برخی از کسانی که به ما اعتماد کردند</h2>

        </div>
        <div class="row">
            @foreach($customers as $customer)
            <a class="col single-img" href="#">
                @if(!$customer->Hasmedia('images'))
                    <img  src="{{asset('img/no-img.gif')}}" alt="" class="img-fluid d-block mx-auto" style="width: 80px">


                @else
                    <img src="{{$customer->getFirstMediaUrl('images')}}" alt="" class="img-fluid d-block mx-auto" style="width: 80px">
                @endif
            </a>
            @endforeach

        </div>
    </div>
</section>
<!-- End brand Area -->
@endif
