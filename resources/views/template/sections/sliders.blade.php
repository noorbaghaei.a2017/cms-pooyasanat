<!-- Start Main Banner Area -->
<div class="home-slides owl-carousel owl-theme">
    @foreach($sliders as $slider)

            @if($slider->Hasmedia('images'))
            <div class="main-banner item-bg1" style="background-image: url({{$slider->getFirstMediaUrl('images')}})">
                @else
                    <div class="main-banner item-bg1">
            @endif
  </div>
    @endforeach
</div>
<!-- End Main Banner Area -->
