<div class="main_menu">
    <nav class="navbar navbar-expand-lg navbar-light main_box">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <a class="navbar-brand logo_h" href="{{route('front.website')}}">

                @if(!$setting->Hasmedia('logo'))

                    <img src="{{asset('img/no-img.gif')}}" alt="" width="80">
                @else
                    <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="" width="80">

                @endif


            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                <ul class="nav navbar-nav menu_nav ml-auto">
@foreach($top_menus as $menu)
                    <li class="nav-item"><a class="nav-link" href="{{$menu->href}}">{{$menu->symbol}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </nav>
</div>
