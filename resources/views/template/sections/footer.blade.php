
<!-- start footer Area -->
<footer class="footer-area section_gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-6 col-sm-12">
                <div class="single-footer-widget text-right" style="direction: rtl">
                    <h6>ما را دنبال کنید</h6>
                    <div class="footer-social d-flex align-items-center">
                        <a href="https://www.instagram.com/{{$setting->info->instagram}}"><i class="fa fa-instagram"></i></a>
                        <a href="https://wa.me/{{$setting->info->whatsapp}}"><i class="fa fa-whatsapp"></i></a>
                        <a href="https://telegram.me/{{$setting->info->telegram}}"><i class="fa fa-telegram"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-8  col-md-6 col-sm-12">
                <div class="single-footer-widget text-center">
                    <h5>در کنار شما تا رسیدن به سلامت همگانی</h5>
                    <h6>حافظان سلامت پویا صنعت</h6>
                    <p>مدیر عامل : دکتر پر همت <a href="tel:09101398132" style="color:#fff"><i class="fa fa-phone"></i></a></p>
                    <p> مدیر فروش تهران : مهندس تقی زاده <a href="tel:09392433323" style="color:#fff"><i class="fa fa-phone"></i></a></p>
                    <p> مدیر فروش لرستان : مهندس بختیاری<a href="tel:09354705997" style="color:#fff" ><i class="fa fa-phone"></i></a></p>
                    <p> مدیر فروش شمال کشور : مهندس یعقوبی <a href="tel:09020542080" style="color:#fff"><i class="fa fa-phone"></i></a></p>
                    <p>مدیر فروش همدان : مهندس غفاری <a href="tel:09184967547" style="color:#fff"><i class="fa fa-phone"></i></a></p>
                    <p>مدیر فروش فارس  <a href="tel:09128432962" style="color:#fff"><i class="fa fa-phone"></i></a></p>
                    <p>مدیر فروش آذربایجان شرقی  <a href="tel:09144100097" style="color:#fff"><i class="fa fa-phone"></i></a></p>
                    <p>مدیر فروش کرمان  <a href="tel:09131405080" style="color:#fff"><i class="fa fa-phone"></i></a></p>

                </div>
            </div>

            <div class="col-lg-2 col-md-6 col-sm-12">
                <div class="single-footer-widget text-right">
                    <h6>درباره ما</h6>
                    <p>
{{--                        <span>{{callingTranslateCountry($setting->country,'fa')}}</span>--}}
                       هلدینگ پویا صنعت
                    </p>
                    <p>
                        مستقیم و با قیمت واقعی از تولید کنندگان معتبر کشور خرید کنید یا ( از ما خرید کنید )
                    </p>
                    <p>
                        تمامی محصولات هلدینگ پویا صنعت دارای گارانتی برگشت محصول می باشند ، با خیال راحت خرید کنید
                    </p>

                </div>
            </div>
        </div>
        <div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
            <p class="footer-text m-0">
                کلیه حقوق متعلق و محفوظ  هلدینگ پویاصنعت  می باشد
            </p>
        </div>
    </div>
</footer>
<!-- End footer Area -->

<script src="{{asset('template/js/vendor/jquery-2.2.4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="{{asset('template/js/vendor/bootstrap.min.js')}}"></script>
<script src="{{asset('template/js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('template/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('template/js/jquery.sticky.js')}}"></script>
<script src="{{asset('template/js/nouislider.min.js')}}"></script>
<script src="{{asset('template/js/countdown.js')}}"></script>
<script src="{{asset('template/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('template/js/owl.carousel.min.js')}}"></script>
<!--gmaps Js-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
</body>

@yield('script')

</html>
