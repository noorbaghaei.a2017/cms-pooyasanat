<!DOCTYPE html>
<html lang="fa">

<head>
    <meta charset="utf-8" />
    {!! SEO::generate() !!}

    @yield('seo')
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="JB desks,job portal,job" />
    <meta name="keywords" content="JB desks,job portal,job" />
    <meta name="author" content="" />
    <meta name="MobileOptimized" content="320" />
    <!--Template style -->
    <link rel="stylesheet" type="text/css" href="{{asset('template/user/css/animate.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/user/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/user/css/fonts.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/user/css/flaticon.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/user/css/font-awesome.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/user/css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/user/css/owl.theme.default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/user/css/magnific-popup.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/user/css/dropify.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/user/css/nice-select.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/user/css/jquery-ui.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('template/user/css/reset.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/user/css/style.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('template/user/css/responsive.css')}}" />

    @yield('head')
    <!--favicon-->
    <link rel="shortcut icon" type="image/png" href="{{asset('template/user/images/favicon.png')}}" />
</head>

<body>
<!-- preloader Start -->
<!-- preloader Start -->
<div class="jb_preloader">
    <div class="spinner_wrap">
        <div class="spinner"></div>
    </div>
</div>
<div class="cursor"></div>
<!-- Top Scroll Start --><a href="javascript:" id="return-to-top"><i class="fas fa-angle-double-up"></i></a>
<!-- Top Scroll End -->
<!-- cp navi wrapper Start -->
<nav class="cd-dropdown  d-block d-sm-block d-md-block d-lg-none d-xl-none">
    <h2><a href="index.html"> <span><img src="images/logo.png" alt="img"></span></a></h2>
    <a href="#0" class="cd-close">بستن</a>
    <ul class="cd-dropdown-content">
        <li>
            <form class="cd-search">
                <input type="search" placeholder="جستجو...">
            </form>
        </li>

        <li class="has-children">
            <a href="#">مشاغل</a>
            <ul class="cd-secondary-dropdown icon_menu is-hidden">
                <li class="go-back"><a href="#0">منو</a></li>
                <li><a href="../main_pages/job_listing_grid_left_filter.html">آگهی شغلی گرید </a></li>
                <li><a href="../main_pages/job_listing_list_left_filter.html">آگهی شغلی لیست</a></li>
                <li><a href="../main_pages/job_single.html">شغل تنها</a></li>
            </ul>
        </li>
        <!-- .has-children -->

        <li class="has-children">
            <a href="#">داشبورد</a>
            <ul class="cd-secondary-dropdown is-hidden">
                <li class="go-back"><a href="#0">منو</a>
                </li>
                <li class="has-children"> <a href="#">داوطلب</a>
                    <ul class="cd-secondary-dropdown is-hidden">
                        <li class="go-back"><a href="#0">منو</a>
                        </li>
                        <li>
                            <a href="../dashhttp://127.0.0.1:8000/template/images/profile.jpgboard/candidate_applied_job.html">شغل تاییده شده </a>
                        </li>
                        <li>
                            <a href="../dashboard/candidate_dashboard.html">داشبورد</a>
                        </li>
                        <li>
                            <a href="../dashboard/candidate_edit_profile.html"> ویرایش پروفایل</a>
                        </li>
                        <li>
                            <a href="../dashboard/candidate_favourite_job.html">شغل موردعلاقه</a>
                        </li>
                        <li>
                            <a href="../dashboard/candidate_resume.html"> رزومه</a>
                        </li>
                        <li>
                            <a href="../dashboard/message.html"> پیام</a>
                        </li>
                        <li>
                            <a href="../dashboard/pricing_plans.html">برنامه های قیمت گذاری</a>
                        </li>
                    </ul>
                    <!-- .cd-secondary-dropdown -->
                </li>
                <!-- .has-children -->
                <li class="has-children"> <a href="#">شرکت</a>
                    <ul class="cd-secondary-dropdown is-hidden">
                        <li class="go-back"><a href="#0">منو</a>
                        </li>
                        <li>
                            <a href="../dashboard/comp_applications.html"> برنامه ها </a>
                        </li>
                        <li>
                            <a href="../dashboard/comp_company_page.html">صفحه شرکت</a>
                        </li>
                        <li>
                            <a href="../dashboard/comp_employer_dashboard.html"> داشبورد</a>
                        </li>
                        <li>
                            <a href="../dashboard/comp_employer_edit_profile.html">ویرایش پروفایل</a>
                        </li>
                        <li>
                            <a href="../dashboard/comp_employer_manage_jobs.html"> مدیریت مشاغل</a>
                        </li>
                        <li>
                            <a href="../dashboard/comp_post_new_job.html"> ارسال شغل جدید</a>
                        </li>
                        <li>
                            <a href="../dashboard/message.html">پیام</a>
                        </li>
                        <li>
                            <a href="../dashboard/pricing_plans.html">برنامه های قیمت گذاری</a>
                        </li>
                    </ul>
                    <!-- .cd-secondary-dropdown -->
                </li>
            </ul>
            <!-- .cd-secondary-dropdown -->
        </li>
        <li class="has-children">
            <a href="#">وبلاگ</a>
            <ul class="cd-secondary-dropdown icon_menu is-hidden">
                <li class="go-back"><a href="#0">منو</a></li>
                <li><a href="../main_pages/blog_single.html">وبلاگ تنها</a></li>
                <li><a href="../main_pages/blog_category_right_sidebar.html">دسته بندی وبلاگ</a></li>
            </ul>
        </li>
        <li><a href="../main_pages/contact_us.html">تماس با ما </a></li>
        <li><a href="../main_pages/login.html">ورود</a></li>
    </ul>
    <!-- .cd-dropdown-content -->
</nav>
<div class="cp_navi_main_wrapper jb_cover">
    <div class="container-fluid">
        <div class="cp_logo_wrapper">
            <a href="{{route('front.website')}}">
                <img src="{{asset('template/user/images/logo.png')}}" alt="logo">
            </a>
        </div>
        <!-- mobile menu area start -->
        <header class="mobail_menu d-block d-sm-block d-md-block d-lg-none d-xl-none">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="cd-dropdown-wrapper">
                            <a class="house_toggle" href="#0">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 31.177 31.177" style="enable-background:new 0 0 31.177 31.177;" xml:space="preserve" width="25px" height="25px">
                                        <g>
                                            <g>
                                                <path class="menubar" d="M30.23,1.775H0.946c-0.489,0-0.887-0.398-0.887-0.888S0.457,0,0.946,0H30.23    c0.49,0,0.888,0.398,0.888,0.888S30.72,1.775,30.23,1.775z" fill="#004165" />
                                            </g>
                                            <g>
                                                <path class="menubar" d="M30.23,9.126H12.069c-0.49,0-0.888-0.398-0.888-0.888c0-0.49,0.398-0.888,0.888-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,8.729,30.72,9.126,30.23,9.126z" fill="#004165" />
                                            </g>
                                            <g>
                                                <path class="menubar" d="M30.23,16.477H0.946c-0.489,0-0.887-0.398-0.887-0.888c0-0.49,0.398-0.888,0.887-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,16.079,30.72,16.477,30.23,16.477z" fill="#004165" />
                                            </g>
                                            <g>
                                                <path class="menubar" d="M30.23,23.826H12.069c-0.49,0-0.888-0.396-0.888-0.887c0-0.49,0.398-0.888,0.888-0.888H30.23    c0.49,0,0.888,0.397,0.888,0.888C31.118,23.43,30.72,23.826,30.23,23.826z" fill="#004165" />
                                            </g>
                                            <g>
                                                <path class="menubar" d="M30.23,31.177H0.946c-0.489,0-0.887-0.396-0.887-0.887c0-0.49,0.398-0.888,0.887-0.888H30.23    c0.49,0,0.888,0.398,0.888,0.888C31.118,30.78,30.72,31.177,30.23,31.177z" fill="#004165" />
                                            </g>
                                        </g>
                                    </svg>
                            </a>
                            <!-- .cd-dropdown -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- .cd-dropdown-wrapper -->
        </header>
        @include('template.sections.status')

        @include('template.sections.menu')
    </div>
</div>
<!-- navi wrapper End -->
