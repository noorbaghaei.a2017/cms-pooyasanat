<!-- Three -->
<section id="three" class="wrapper">
    <div class="inner flex flex-3">
        @foreach($new_articles as $article)
        <div class="flex-item box">
            <div class="image fit">
                <a href="{{route('articles.single',['article'=>$article->slug])}}">
                @if(!$article->Hasmedia('images'))
                    <img src="{{asset('img/no-img.gif')}}" alt="" >
                @else

                    <img src="{{$article->getFirstMediaUrl('images')}}" alt="" >
                @endif
                </a>
            </div>
            <div class="content">
                <h3>{{$article->title}}</h3>
                <p>{{$article->excerpt}}</p>
            </div>
        </div>
        @endforeach

    </div>
</section>

