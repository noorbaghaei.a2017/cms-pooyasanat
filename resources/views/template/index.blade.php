﻿@extends('template.app')

@section('content')
    <!-- start banner Area -->
    <section class="banner-area">
        <div class="container">
            <div class="row fullscreen align-items-center justify-content-start">
                <div class="col-lg-12">
                    <div class="active-banner-slider owl-carousel">
                        <!-- single-slide -->
                        <div class="row single-slide align-items-center d-flex">
                            <div class="col-lg-5 col-md-6">
                                <div class="banner-content">
                                    <h1>هلدینگ پویا صنعت </h1>
                                    <p>توضیحاتی در مورد هلدینگ</p>

                                </div>
                            </div>
                        </div>
                        <!-- single-slide -->
                        <div class="row single-slide">
                            <div class="col-lg-5">
                                <div class="banner-content">
                                    <h1>هلدینگ پویا صنعت </h1>
                                    <p>توضیحاتی در مورد هلدینگ</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->


    <!-- start features Area -->
    <section class="features-area section_gap">
        <div class="container">
            <div class="row features-inner" style="direction: rtl">
                @foreach($original_category_products as $category)
                <!-- single features -->
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <form method="GET" action="{{route('filter.shop')}}">
                            @csrf
                            <input hidden class="pixel-radio" name="category[]" checked value="{{$category->id}}" type="checkbox" >
                            <div class="single-features">
                                <div class="f-icon">
                                    {{--                            <img src="{{asset('template/img/features/f-icon1.png')}}" alt="">--}}
                                </div>
                                <button type="submit" style="cursor: pointer;border: none;background: transparent">
                                    <h6>{{$category->symbol}}</h6>
                                </button>
                            </div>
                        </form>
                    </div>

                    @endforeach

            </div>
        </div>
    </section>
    <!-- end features Area -->

    <!-- start features Area -->
    <section class="features-area section_gap">
        <div class="container">
            <div class="row features-inner">
                <!-- single features -->
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-features">
                        <div class="f-icon">
                            <img src="{{asset('template/img/features/f-icon1.png')}}" alt="">
                        </div>
                        <h6>ارسال رایگان</h6>
                    </div>
                </div>
                <!-- single features -->
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-features">
                        <div class="f-icon">
                            <img src="{{asset('template/img/features/f-icon2.png')}}" alt="">
                        </div>
                        <h6>بازگشت وجه</h6>

                    </div>
                </div>
                <!-- single features -->
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-features">
                        <div class="f-icon">
                            <img src="{{asset('template/img/features/f-icon3.png')}}" alt="">
                        </div>
                        <h6>24/7 پشتیبانی</h6>

                    </div>
                </div>
                <!-- single features -->
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-features">
                        <div class="f-icon">
                            <img src="{{asset('template/img/features/f-icon4.png')}}" alt="">
                        </div>
                        <h6>پرداخت ایمن</h6>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end features Area -->

    <!-- Start category Area -->
    <section class="category-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-6">
                    <div class="single-deal">
                        <div class="overlay"></div>
                        <img class="img-fluid w-100" src="" alt="">
                        <a href="#" class="img-pop-up" target="_blank">
                            <div class="deal-details">
                                <h6 class="deal-title">ماسک</h6>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12">
                    <div class="row">

                        <div class="col-lg-8 col-md-8">
                            <div class="text-center bg-black">
                               <a href="{{env('APP_URL')}}/shop/filter?_token=UwLrSwXu5HELXYyuGdd35EFaIv5shr2p6KtDdenZ&category%5B%5D=18&show=new">
                                   <h6>نفت و مشتقات نفتی</h6>
                               </a>
                            </div>
                            <div class="single-deal">
                                <div class="overlay"></div>

                                <img class="img-fluid w-100" src="{{asset('template/img/home/bg-1-home.jpeg')}}" alt="">

                                <a href="{{asset('template/img/home/bg-1-home.jpeg')}}" class="img-pop-up" target="_blank">
                                    <div class="deal-details">
                                        <h6 class="deal-title"> گالری تصاویر</h6>
                                    </div>
                                </a>
                            </div>

                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="text-center bg-black">
                                <a href="#">
                                    <h6>دستکش</h6>
                                </a>
                            </div>
                            <div class="single-deal">
                                <div class="overlay"></div>
                                <img class="img-fluid w-100" src="{{asset('template/img/dastkesh.jpeg')}}" alt="">
                                <a href="{{asset('template/img/dastkesh.jpg')}}" class="img-pop-up" target="_blank">
                                    <div class="deal-details">
                                        <h6 class="deal-title"> گالری تصاویر</h6>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <div class="text-center bg-black">
                                <a href="{{env('APP_URL')}}/shop/filter?_token=37a1CZHhsex3EvLTWughHePdJGC7JqsAxtW1vHjO&category%5B%5D=11&show=new">
                                    <h6>الکل طبی</h6>
                                </a>
                            </div>
                            <div class="single-deal">
                                <div class="overlay"></div>
                                <img class="img-fluid w-100" src="{{asset('template/img/etanol.jpeg')}}" alt="">
                                <a href="{{asset('template/img/slider-etanol.jpg')}}" class="img-pop-up" target="_blank">
                                    <div class="deal-details">
                                        <h6 class="deal-title"> گالری تصاویر</h6>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="text-center bg-black">
                                <a href="{{env('APP_URL')}}/shop/filter?_token=37a1CZHhsex3EvLTWughHePdJGC7JqsAxtW1vHjO&category%5B%5D=10&show=new">
                                    <h6>ماسک</h6>
                                </a>
                            </div>
                            <div class="single-deal">
                                <div class="overlay"></div>
                                <img class="img-fluid w-100" src="{{asset('template/img/mask.jpg')}}" alt="">
                                <a href="{{asset('template/img/mask.jpg')}}" class="img-pop-up" target="_blank">
                                    <div class="deal-details">
                                        <h6 class="deal-title"> گالری تصاویر</h6>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End category Area -->

@include('template.sections.products')

    <!-- Start exclusive deal Area -->
    <section class="exclusive-deal-area">
        <div class="container-fluid">
            <div class="row justify-content-center align-items-center">

                <div class="col-lg-6 no-padding exclusive-right">
                    <h1>محصولات پربازدید</h1>
                    <div class="active-exclusive-product-slider">
                        @foreach($popular_products as $popular)
                        <!-- single exclusive carousel -->
                        <div class="single-exclusive-slider">
                            <a href="{{route('products.single',['product'=>$popular->slug])}}">
                                @if(!$popular->Hasmedia('images'))
                                    <img class="img-fluid" src="{{asset('img/no-img.gif')}}" alt="">
                                @else
                                    <img class="img-fluid" src="{{$popular->getFirstMediaUrl('images')}}" alt="">

                                @endif

                            </a>
                                <div class="product-details" style="margin-top: 10px">
                                    <a href="{{route('products.single',['product'=>$popular->slug])}}">
                                <h4>{{$popular->title}}</h4>
                                    </a>

                            </div>
                        </div>
                            @endforeach
                    </div>
                </div>
                <div class="col-lg-6 no-padding exclusive-left">
                    <div class="row clock_sec clockdiv" id="clockdiv">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End exclusive deal Area -->

@include('template.sections.customers')

@endsection

@section('script')
    <script>


        $(document).ready(function(){
            "use strict";

            var window_width 	 = $(window).width(),
                window_height 		 = window.innerHeight,
                header_height 		 = $(".default-header").height(),
                header_height_static = $(".site-header.static").outerHeight(),
                fitscreen 			 = window_height - header_height;


            $(".fullscreen").css("height", window_height)
            $(".fitscreen").css("height", fitscreen);

            //------- Active Nice Select --------//

            $('select').niceSelect();


            $('.navbar-nav li.dropdown').hover(function() {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
            }, function() {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
            });

            $('.img-pop-up').magnificPopup({
                type: 'image',
                gallery:{
                    enabled:true
                }
            });

            // Search Toggle
            $("#search_input_box").hide();
            $("#search").on("click", function () {
                $("#search_input_box").slideToggle();
                $("#search_input").focus();
            });
            $("#close_search").on("click", function () {
                $('#search_input_box').slideUp(500);
            });

            /*==========================
                javaScript for sticky header
                ============================*/
            $(".sticky-header").sticky();

            /*=================================
            Javascript for banner area carousel
            ==================================*/
            $(".active-banner-slider").owlCarousel({
                items:1,
                autoplay:false,
                autoplayTimeout: 5000,
                loop:true,
                nav:true,
                navText:["<img src='{{asset('template/img/back-icon.png')}}' width='40'>","<img src='{{asset('template/img/next-icon.png')}}' width='40'>"],
                dots:false
            });

            /*=================================
            Javascript for product area carousel
            ==================================*/
            $(".active-product-area").owlCarousel({
                items:1,
                autoplay:false,
                autoplayTimeout: 5000,
                loop:true,
                nav:true,
                navText:["<img src='{{asset('template/img/back-icon.png')}}' width='40'>","<img src='{{asset('template/img/next-icon.png')}}' width='40'>"],
                dots:false
            });

            /*=================================
            Javascript for single product area carousel
            ==================================*/
            $(".s_Product_carousel").owlCarousel({
                items:1,
                autoplay:false,
                autoplayTimeout: 5000,
                loop:true,
                nav:false,
                dots:true
            });

            /*=================================
            Javascript for exclusive area carousel
            ==================================*/
            $(".active-exclusive-product-slider").owlCarousel({
                items:1,
                autoplay:false,
                autoplayTimeout: 5000,
                loop:true,
                nav:true,
                navText:["<img src='{{asset('template/img/back-icon.png')}}' width='40'>","<img src='{{asset('template/img/next-icon.png')}}' width='40'>"],
                dots:false
            });

            //--------- Accordion Icon Change ---------//

            $('.collapse').on('shown.bs.collapse', function(){
                $(this).parent().find(".lnr-arrow-right").removeClass("lnr-arrow-right").addClass("lnr-arrow-left");
            }).on('hidden.bs.collapse', function(){
                $(this).parent().find(".lnr-arrow-left").removeClass("lnr-arrow-left").addClass("lnr-arrow-right");
            });

            // Select all links with hashes
            $('.main-menubar a[href*="#"]')
                // Remove links that don't actually link to anything
                .not('[href="#"]')
                .not('[href="#0"]')
                .click(function(event) {
                    // On-page links
                    if (
                        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                        &&
                        location.hostname == this.hostname
                    ) {
                        // Figure out element to scroll to
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        // Does a scroll target exist?
                        if (target.length) {
                            // Only prevent default if animation is actually gonna happen
                            event.preventDefault();
                            $('html, body').animate({
                                scrollTop: target.offset().top-70
                            }, 1000, function() {
                                // Callback after animation
                                // Must change focus!
                                var $target = $(target);
                                $target.focus();
                                if ($target.is(":focus")) { // Checking if the target was focused
                                    return false;
                                } else {
                                    $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                                    $target.focus(); // Set focus again
                                };
                            });
                        }
                    }
                });



            // -------   Mail Send ajax

            $(document).ready(function() {
                var form = $('#booking'); // contact form
                var submit = $('.submit-btn'); // submit button
                var alert = $('.alert-msg'); // alert div for show alert message

                // form submit event
                form.on('submit', function(e) {
                    e.preventDefault(); // prevent default form submit

                    $.ajax({
                        url: 'booking.php', // form action url
                        type: 'POST', // form submit method get/post
                        dataType: 'html', // request type html/json/xml
                        data: form.serialize(), // serialize form data
                        beforeSend: function() {
                            alert.fadeOut();
                            submit.html('Sending....'); // change submit button text
                        },
                        success: function(data) {
                            alert.html(data).fadeIn(); // fade in response data
                            form.trigger('reset'); // reset form
                            submit.attr("style", "display: none !important");; // reset submit button text
                        },
                        error: function(e) {
                            console.log(e)
                        }
                    });
                });
            });




            $(document).ready(function() {
                $('#mc_embed_signup').find('form').ajaxChimp();
            });



            if(document.getElementById("js-countdown")){

                var countdown = new Date("October 17, 2018");

                function getRemainingTime(endtime) {
                    var milliseconds = Date.parse(endtime) - Date.parse(new Date());
                    var seconds = Math.floor(milliseconds / 1000 % 60);
                    var minutes = Math.floor(milliseconds / 1000 / 60 % 60);
                    var hours = Math.floor(milliseconds / (1000 * 60 * 60) % 24);
                    var days = Math.floor(milliseconds / (1000 * 60 * 60 * 24));

                    return {
                        'total': milliseconds,
                        'seconds': seconds,
                        'minutes': minutes,
                        'hours': hours,
                        'days': days
                    };
                }

                function initClock(id, endtime) {
                    var counter = document.getElementById(id);
                    var daysItem = counter.querySelector('.js-countdown-days');
                    var hoursItem = counter.querySelector('.js-countdown-hours');
                    var minutesItem = counter.querySelector('.js-countdown-minutes');
                    var secondsItem = counter.querySelector('.js-countdown-seconds');

                    function updateClock() {
                        var time = getRemainingTime(endtime);

                        daysItem.innerHTML = time.days;
                        hoursItem.innerHTML = ('0' + time.hours).slice(-2);
                        minutesItem.innerHTML = ('0' + time.minutes).slice(-2);
                        secondsItem.innerHTML = ('0' + time.seconds).slice(-2);

                        if (time.total <= 0) {
                            clearInterval(timeinterval);
                        }
                    }

                    updateClock();
                    var timeinterval = setInterval(updateClock, 1000);
                }

                initClock('js-countdown', countdown);

            };



            $('.quick-view-carousel-details').owlCarousel({
                loop: true,
                dots: true,
                items: 1,
            })



            //----- Active No ui slider --------//



            $(function(){

                if(document.getElementById("price-range")){

                    var nonLinearSlider = document.getElementById('price-range');

                    noUiSlider.create(nonLinearSlider, {
                        connect: true,
                        behaviour: 'tap',
                        start: [ 500, 4000 ],
                        range: {
                            // Starting at 500, step the value by 500,
                            // until 4000 is reached. From there, step by 1000.
                            'min': [ 0 ],
                            '10%': [ 500, 500 ],
                            '50%': [ 4000, 1000 ],
                            'max': [ 10000 ]
                        }
                    });


                    var nodes = [
                        document.getElementById('lower-value'), // 0
                        document.getElementById('upper-value')  // 1
                    ];

                    // Display the slider value and how far the handle moved
                    // from the left edge of the slider.
                    nonLinearSlider.noUiSlider.on('update', function ( values, handle, unencoded, isTap, positions ) {
                        nodes[handle].innerHTML = values[handle];
                    });

                }

            });


            //-------- Have Cupon Button Text Toggle Change -------//

            $('.have-btn').on('click', function(e){
                e.preventDefault();
                $('.have-btn span').text(function(i, text){
                    return text === "Have a Coupon?" ? "Close Coupon" : "Have a Coupon?";
                })
                $('.cupon-code').fadeToggle("slow");
            });

            $('.load-more-btn').on('click', function(e){
                e.preventDefault();
                $('.load-product').fadeIn('slow');
                $(this).fadeOut();
            });





            //------- Start Quantity Increase & Decrease Value --------//




            var value,
                quantity = document.getElementsByClassName('quantity-container');

            function createBindings(quantityContainer) {
                var quantityAmount = quantityContainer.getElementsByClassName('quantity-amount')[0];
                var increase = quantityContainer.getElementsByClassName('increase')[0];
                var decrease = quantityContainer.getElementsByClassName('decrease')[0];
                increase.addEventListener('click', function () { increaseValue(quantityAmount); });
                decrease.addEventListener('click', function () { decreaseValue(quantityAmount); });
            }

            function init() {
                for (var i = 0; i < quantity.length; i++ ) {
                    createBindings(quantity[i]);
                }
            };

            function increaseValue(quantityAmount) {
                value = parseInt(quantityAmount.value, 10);

                console.log(quantityAmount, quantityAmount.value);

                value = isNaN(value) ? 0 : value;
                value++;
                quantityAmount.value = value;
            }

            function decreaseValue(quantityAmount) {
                value = parseInt(quantityAmount.value, 10);

                value = isNaN(value) ? 0 : value;
                if (value > 0) value--;

                quantityAmount.value = value;
            }

            init();

//------- End Quantity Increase & Decrease Value --------//

            /*----------------------------------------------------*/
            /*  Google map js
              /*----------------------------------------------------*/

            if ($("#mapBox").length) {
                var $lat = $("#mapBox").data("lat");
                var $lon = $("#mapBox").data("lon");
                var $zoom = $("#mapBox").data("zoom");
                var $marker = $("#mapBox").data("marker");
                var $info = $("#mapBox").data("info");
                var $markerLat = $("#mapBox").data("mlat");
                var $markerLon = $("#mapBox").data("mlon");
                var map = new GMaps({
                    el: "#mapBox",
                    lat: $lat,
                    lng: $lon,
                    scrollwheel: false,
                    scaleControl: true,
                    streetViewControl: false,
                    panControl: true,
                    disableDoubleClickZoom: true,
                    mapTypeControl: false,
                    zoom: $zoom,
                    styles: [
                        {
                            featureType: "water",
                            elementType: "geometry.fill",
                            stylers: [
                                {
                                    color: "#dcdfe6"
                                }
                            ]
                        },
                        {
                            featureType: "transit",
                            stylers: [
                                {
                                    color: "#808080"
                                },
                                {
                                    visibility: "off"
                                }
                            ]
                        },
                        {
                            featureType: "road.highway",
                            elementType: "geometry.stroke",
                            stylers: [
                                {
                                    visibility: "on"
                                },
                                {
                                    color: "#dcdfe6"
                                }
                            ]
                        },
                        {
                            featureType: "road.highway",
                            elementType: "geometry.fill",
                            stylers: [
                                {
                                    color: "#ffffff"
                                }
                            ]
                        },
                        {
                            featureType: "road.local",
                            elementType: "geometry.fill",
                            stylers: [
                                {
                                    visibility: "on"
                                },
                                {
                                    color: "#ffffff"
                                },
                                {
                                    weight: 1.8
                                }
                            ]
                        },
                        {
                            featureType: "road.local",
                            elementType: "geometry.stroke",
                            stylers: [
                                {
                                    color: "#d7d7d7"
                                }
                            ]
                        },
                        {
                            featureType: "poi",
                            elementType: "geometry.fill",
                            stylers: [
                                {
                                    visibility: "on"
                                },
                                {
                                    color: "#ebebeb"
                                }
                            ]
                        },
                        {
                            featureType: "administrative",
                            elementType: "geometry",
                            stylers: [
                                {
                                    color: "#a7a7a7"
                                }
                            ]
                        },
                        {
                            featureType: "road.arterial",
                            elementType: "geometry.fill",
                            stylers: [
                                {
                                    color: "#ffffff"
                                }
                            ]
                        },
                        {
                            featureType: "road.arterial",
                            elementType: "geometry.fill",
                            stylers: [
                                {
                                    color: "#ffffff"
                                }
                            ]
                        },
                        {
                            featureType: "landscape",
                            elementType: "geometry.fill",
                            stylers: [
                                {
                                    visibility: "on"
                                },
                                {
                                    color: "#efefef"
                                }
                            ]
                        },
                        {
                            featureType: "road",
                            elementType: "labels.text.fill",
                            stylers: [
                                {
                                    color: "#696969"
                                }
                            ]
                        },
                        {
                            featureType: "administrative",
                            elementType: "labels.text.fill",
                            stylers: [
                                {
                                    visibility: "on"
                                },
                                {
                                    color: "#737373"
                                }
                            ]
                        },
                        {
                            featureType: "poi",
                            elementType: "labels.icon",
                            stylers: [
                                {
                                    visibility: "off"
                                }
                            ]
                        },
                        {
                            featureType: "poi",
                            elementType: "labels",
                            stylers: [
                                {
                                    visibility: "off"
                                }
                            ]
                        },
                        {
                            featureType: "road.arterial",
                            elementType: "geometry.stroke",
                            stylers: [
                                {
                                    color: "#d6d6d6"
                                }
                            ]
                        },
                        {
                            featureType: "road",
                            elementType: "labels.icon",
                            stylers: [
                                {
                                    visibility: "off"
                                }
                            ]
                        },
                        {},
                        {
                            featureType: "poi",
                            elementType: "geometry.fill",
                            stylers: [
                                {
                                    color: "#dadada"
                                }
                            ]
                        }
                    ]
                });
            }




        });

    </script>

@endsection


