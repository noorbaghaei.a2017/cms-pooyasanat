@extends('template.app')

@section('content')

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">

            </div>
        </div>
    </section>
    <!-- End Banner Area -->
    <div class="container result-box">
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-md-7">
                <!-- Start Filter Bar -->
                <div class="filter-bar d-flex flex-wrap align-items-center text-right" style="height: 50px">


                </div>
                <!-- End Filter Bar -->
                <!-- Start Best Seller -->
                <section class="lattest-product-area pb-40 category-list">
                    <div class="row" style="direction: rtl">
                        @if(!isset($query))
                        @foreach($items as $item)
                        <!-- single product -->
                        <div class="col-lg-4 col-md-6">
                            <div class="single-product text-right">
                                <a href="{{route('products.single',['product'=>$item->slug])}}">

                                    @if(!$item->Hasmedia('images'))
                                        <img class="img-fluid" src="{{asset('img/no-img.gif')}}" alt="">
                                    @else
                                        <img class="img-fluid" src="{{$item->getFirstMediaUrl('images')}}" alt="">

                                    @endif
                                </a>
                                <div class="product-details">
                                    <a href="{{route('products.single',['product'=>$item->slug])}}">
                                    <h6>{{$item->title}}</h6>
                                    </a>
                                    <div class="price">
                                        <h6>تماس</h6>
                                        <a href="tel::{{\Modules\Core\Entities\User::find($item->user)->mobile}}" class="btn btn-gold"> <span class="fa fa-phone"></span></a>
                                    </div>

                                </div>
                            </div>
                        </div>
                            @endforeach

                            @elseif(isset($query) && count($query) > 0)

                           @foreach($query as $value)

                               <!-- single product -->
                                   <div class="col-lg-4 col-md-6">
                                       <div class="single-product text-right">
                                           <a href="{{route('products.single',['product'=>$value->slug])}}">

                                               @if(!$value->Hasmedia('images'))
                                                   <img class="img-fluid" src="{{asset('img/no-img.gif')}}" alt="">
                                               @else
                                                   <img class="img-fluid" src="{{$value->getFirstMediaUrl('images')}}" alt="">

                                               @endif
                                           </a>
                                           <div class="product-details">
                                               <a href="{{route('products.single',['product'=>$value->slug])}}">
                                                   <h6>{{$value->title}}</h6>
                                               </a>
                                               <div class="price">
                                                   <h6>تماس</h6>
                                                   <a href="tel::{{\Modules\Core\Entities\User::find($value->user)->mobile}}" class="btn btn-gold"> <span class="fa fa-phone"></span></a>
                                               </div>

                                           </div>
                                       </div>
                                   </div>

                                @endforeach

                            @else

                            <!-- single product -->
                                <div class="col-lg-12 col-md-6">
                                    <div class="single-product text-center">
                                      <p>
                                          متاسفانه محصولی یافت نشد
                                      </p>
                                    </div>
                                </div>

                            @endif

                    </div>
                </section>
                <!-- End Best Seller -->


                   @if(isset($query))
                       <!-- Start Filter Bar -->
                           <div class="filter-bar d-flex flex-wrap align-items-center">
                        <div class="pagination">
                            {!! $query->links() !!}
                        </div>
                           </div>
                           <!-- End Filter Bar -->
                    @elseif(isset($items))
                       <!-- Start Filter Bar -->
                           <div class="filter-bar d-flex flex-wrap align-items-center">
                        <div class="pagination">
                            {!! $items->links() !!}
                        </div>
            </div>
            <!-- End Filter Bar -->

                    @endif

            </div>

            <div class="col-xl-3 col-lg-4 col-md-5">
                            <form method="GET" action="{{route('filter.shop')}}">
                                @csrf
                <div class="sidebar-categories text-right">
                    <div class="head">دسته بندی</div>
                    <ul class="main-categories">

                        @foreach($categories as $category)

                            @if(!hasChildCategory($category->id))

                            <li class="main-nav-list"><a href="#"><span
                                        class="lnr lnr-arrow-right"></span>{{$category->symbol}}<span class="number"> ({{countCategory('product',$category->id)}})</span><input class="pixel-radio" {{isset($query) && in_array($category->id,$array_category) ? "checked " :"" }} name="category[]" value="{{$category->id}}" type="checkbox" ></a>

                                @else


                                    <li class="main-nav-list"><a data-toggle="collapse" href="#child{{$category->id}}" aria-expanded="false" aria-controls="child{{$category->id}}"><span
                                                class="lnr lnr-arrow-right"></span>{{$category->symbol}}<span class="number"> ({{countTotalCategory('product',$category->id)}})</span><i class="fa fa-arrow-down" style="padding: 12px"></i></a>


                                        <ul class="collapse" id="child{{$category->id}}" data-toggle="collapse" aria-expanded="false" aria-controls="child{{$category->id}}">
{{--                                            <li class="main-nav-list child"><a href="#" style="padding-right: 30px;padding-left: 0">{{$category->symbol}}<span class="number">({{countTotalCategory('product',$category->id)}})</span><input class="pixel-radio" type="checkbox" name="category[]" value="{{$category->id}}" ></a></li>--}}

                                            @foreach(showChildCategory($category->id) as $child)

                                                @if(!hasChildCategory($child->id))

                                                    <li class="main-nav-list"><a href="#" style="padding-right: 30px;padding-left: 0"><span
                                                                class="lnr lnr-arrow-right"></span>{{$child->symbol}}<span class="number"> ({{countCategory('product',$child->id)}})</span><input class="pixel-radio" {{isset($query) && in_array($child->id,$array_category) ? "checked " :"" }} name="category[]" value="{{$child->id}}" type="checkbox" ></a>


                                                @else



                                                    <li class="main-nav-list"><a style="padding-right: 30px;padding-left: 0" data-toggle="collapse" href="#child{{$child->id}}" aria-expanded="false" aria-controls="child{{$child->id}}"><span
                                                                class="lnr lnr-arrow-right"></span>{{$child->symbol}}<span class="number"> ({{countTotalCategory('product',$child->id)}})</span><i class="fa fa-arrow-down" style="padding: 12px"></i></a>


                                                        <ul class="collapse" id="child{{$child->id}}" data-toggle="collapse" aria-expanded="false" aria-controls="child{{$child->id}}">
{{--                                                            <li class="main-nav-list child"><a href="#" style="padding-right: 50px;padding-left: 0">{{$child->symbol}}<span class="number">({{countTotalCategory('product',$child->id)}})</span><input class="pixel-radio" type="checkbox" name="category[]" value="{{$child->id}}" ></a></li>--}}

                                                            @foreach(showChildCategory($child->id) as $childeren)

                                                                    <li class="main-nav-list child"><a href="#" style="padding-right: 50px;padding-left: 0">{{$childeren->symbol}}<span class="number">({{countCategory('product',$childeren->id)}})</span><input class="pixel-radio"  {{isset($query) && in_array($childeren->id,$array_category) ? "checked " :"" }} type="checkbox" name="category[]" value="{{$childeren->id}}"  ></a></li>

                                                            @endforeach
                                                        </ul>
                                                    </li>


                                                @endif

                                            @endforeach
                                        </ul>
                                    </li>



                                @endif

                            </li>

                        @endforeach

                    </ul>
                </div>
                <div class="sidebar-filter mt-50 text-right">
                    <div class="top-filter-head">نمایش</div>
                    <div class="common-filter">

                            <ul>
                                <li class="filter-list"><label for="new">جدید ترین</label><input class="pixel-radio" checked type="radio" value="new" id="new" name="show"></li>
                            </ul>

                    </div>
                    <div class="common-filter">
                        <div class="head"></div>
                        <input type="submit" class="btn btn-gold" value="اعمال تغییرات">
                    </div>
                </div>
                            </form>
            </div>


        </div>
    </div>

@endsection


@section('script')
    <script>


        $(document).ready(function(){
            "use strict";

            var window_width 	 = $(window).width(),
                window_height 		 = window.innerHeight,
                header_height 		 = $(".default-header").height(),
                header_height_static = $(".site-header.static").outerHeight(),
                fitscreen 			 = window_height - header_height;


            $(".fullscreen").css("height", window_height)
            $(".fitscreen").css("height", fitscreen);

            //------- Active Nice Select --------//

            $('select').niceSelect();


            $('.navbar-nav li.dropdown').hover(function() {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
            }, function() {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
            });

            $('.img-pop-up').magnificPopup({
                type: 'image',
                gallery:{
                    enabled:true
                }
            });

            // Search Toggle
            $("#search_input_box").hide();
            $("#search").on("click", function () {
                $("#search_input_box").slideToggle();
                $("#search_input").focus();
            });
            $("#close_search").on("click", function () {
                $('#search_input_box').slideUp(500);
            });

            /*==========================
                javaScript for sticky header
                ============================*/
            $(".sticky-header").sticky();

            /*=================================
            Javascript for banner area carousel
            ==================================*/
            $(".active-banner-slider").owlCarousel({
                items:1,
                autoplay:false,
                autoplayTimeout: 5000,
                loop:true,
                nav:true,
                navText:["<img src='{{asset('template/img/product/back-icon.png')}}' width='40'>","<img src='{{asset('template/img/product/next-icon.png')}}' width='20'>"],
                dots:false
            });

            /*=================================
            Javascript for product area carousel
            ==================================*/
            $(".active-product-area").owlCarousel({
                items:1,
                autoplay:false,
                autoplayTimeout: 5000,
                loop:true,
                nav:true,
                navText:["<img src='{{asset('template/img/product/back-icon.png')}}' width='40'>","<img src='{{asset('template/img/product/next-icon.png')}}' width='20'>"],
                dots:false
            });

            /*=================================
            Javascript for single product area carousel
            ==================================*/
            $(".s_Product_carousel").owlCarousel({
                items:1,
                autoplay:false,
                autoplayTimeout: 5000,
                loop:true,
                nav:false,
                dots:true
            });

            /*=================================
            Javascript for exclusive area carousel
            ==================================*/
            $(".active-exclusive-product-slider").owlCarousel({
                items:1,
                autoplay:false,
                autoplayTimeout: 5000,
                loop:true,
                nav:true,
                navText:["<img src='{{asset('template/img/product/back-icon.png')}}' width='40'>","<img src='{{asset('template/img/product/next-icon.png')}}' width='20'>"],
                dots:false
            });

            //--------- Accordion Icon Change ---------//

            $('.collapse').on('shown.bs.collapse', function(){
                $(this).parent().find(".lnr-arrow-right").removeClass("lnr-arrow-right").addClass("lnr-arrow-left");
            }).on('hidden.bs.collapse', function(){
                $(this).parent().find(".lnr-arrow-left").removeClass("lnr-arrow-left").addClass("lnr-arrow-right");
            });

            // Select all links with hashes
            $('.main-menubar a[href*="#"]')
                // Remove links that don't actually link to anything
                .not('[href="#"]')
                .not('[href="#0"]')
                .click(function(event) {
                    // On-page links
                    if (
                        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                        &&
                        location.hostname == this.hostname
                    ) {
                        // Figure out element to scroll to
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        // Does a scroll target exist?
                        if (target.length) {
                            // Only prevent default if animation is actually gonna happen
                            event.preventDefault();
                            $('html, body').animate({
                                scrollTop: target.offset().top-70
                            }, 1000, function() {
                                // Callback after animation
                                // Must change focus!
                                var $target = $(target);
                                $target.focus();
                                if ($target.is(":focus")) { // Checking if the target was focused
                                    return false;
                                } else {
                                    $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                                    $target.focus(); // Set focus again
                                };
                            });
                        }
                    }
                });



            // -------   Mail Send ajax

            $(document).ready(function() {
                var form = $('#booking'); // contact form
                var submit = $('.submit-btn'); // submit button
                var alert = $('.alert-msg'); // alert div for show alert message

                // form submit event
                form.on('submit', function(e) {
                    e.preventDefault(); // prevent default form submit

                    $.ajax({
                        url: 'booking.php', // form action url
                        type: 'POST', // form submit method get/post
                        dataType: 'html', // request type html/json/xml
                        data: form.serialize(), // serialize form data
                        beforeSend: function() {
                            alert.fadeOut();
                            submit.html('Sending....'); // change submit button text
                        },
                        success: function(data) {
                            alert.html(data).fadeIn(); // fade in response data
                            form.trigger('reset'); // reset form
                            submit.attr("style", "display: none !important");; // reset submit button text
                        },
                        error: function(e) {
                            console.log(e)
                        }
                    });
                });
            });




            $(document).ready(function() {
                $('#mc_embed_signup').find('form').ajaxChimp();
            });



            if(document.getElementById("js-countdown")){

                var countdown = new Date("October 17, 2018");

                function getRemainingTime(endtime) {
                    var milliseconds = Date.parse(endtime) - Date.parse(new Date());
                    var seconds = Math.floor(milliseconds / 1000 % 60);
                    var minutes = Math.floor(milliseconds / 1000 / 60 % 60);
                    var hours = Math.floor(milliseconds / (1000 * 60 * 60) % 24);
                    var days = Math.floor(milliseconds / (1000 * 60 * 60 * 24));

                    return {
                        'total': milliseconds,
                        'seconds': seconds,
                        'minutes': minutes,
                        'hours': hours,
                        'days': days
                    };
                }

                function initClock(id, endtime) {
                    var counter = document.getElementById(id);
                    var daysItem = counter.querySelector('.js-countdown-days');
                    var hoursItem = counter.querySelector('.js-countdown-hours');
                    var minutesItem = counter.querySelector('.js-countdown-minutes');
                    var secondsItem = counter.querySelector('.js-countdown-seconds');

                    function updateClock() {
                        var time = getRemainingTime(endtime);

                        daysItem.innerHTML = time.days;
                        hoursItem.innerHTML = ('0' + time.hours).slice(-2);
                        minutesItem.innerHTML = ('0' + time.minutes).slice(-2);
                        secondsItem.innerHTML = ('0' + time.seconds).slice(-2);

                        if (time.total <= 0) {
                            clearInterval(timeinterval);
                        }
                    }

                    updateClock();
                    var timeinterval = setInterval(updateClock, 1000);
                }

                initClock('js-countdown', countdown);

            };



            $('.quick-view-carousel-details').owlCarousel({
                loop: true,
                dots: true,
                items: 1,
            })



            //----- Active No ui slider --------//



            $(function(){

                if(document.getElementById("price-range")){

                    var nonLinearSlider = document.getElementById('price-range');

                    noUiSlider.create(nonLinearSlider, {
                        connect: true,
                        behaviour: 'tap',
                        start: [ 500, 4000 ],
                        range: {
                            // Starting at 500, step the value by 500,
                            // until 4000 is reached. From there, step by 1000.
                            'min': [ 0 ],
                            '10%': [ 500, 500 ],
                            '50%': [ 4000, 1000 ],
                            'max': [ 10000 ]
                        }
                    });


                    var nodes = [
                        document.getElementById('lower-value'), // 0
                        document.getElementById('upper-value')  // 1
                    ];

                    // Display the slider value and how far the handle moved
                    // from the left edge of the slider.
                    nonLinearSlider.noUiSlider.on('update', function ( values, handle, unencoded, isTap, positions ) {
                        nodes[handle].innerHTML = values[handle];
                    });

                }

            });


            //-------- Have Cupon Button Text Toggle Change -------//

            $('.have-btn').on('click', function(e){
                e.preventDefault();
                $('.have-btn span').text(function(i, text){
                    return text === "Have a Coupon?" ? "Close Coupon" : "Have a Coupon?";
                })
                $('.cupon-code').fadeToggle("slow");
            });

            $('.load-more-btn').on('click', function(e){
                e.preventDefault();
                $('.load-product').fadeIn('slow');
                $(this).fadeOut();
            });





            //------- Start Quantity Increase & Decrease Value --------//




            var value,
                quantity = document.getElementsByClassName('quantity-container');

            function createBindings(quantityContainer) {
                var quantityAmount = quantityContainer.getElementsByClassName('quantity-amount')[0];
                var increase = quantityContainer.getElementsByClassName('increase')[0];
                var decrease = quantityContainer.getElementsByClassName('decrease')[0];
                increase.addEventListener('click', function () { increaseValue(quantityAmount); });
                decrease.addEventListener('click', function () { decreaseValue(quantityAmount); });
            }

            function init() {
                for (var i = 0; i < quantity.length; i++ ) {
                    createBindings(quantity[i]);
                }
            };

            function increaseValue(quantityAmount) {
                value = parseInt(quantityAmount.value, 10);

                console.log(quantityAmount, quantityAmount.value);

                value = isNaN(value) ? 0 : value;
                value++;
                quantityAmount.value = value;
            }

            function decreaseValue(quantityAmount) {
                value = parseInt(quantityAmount.value, 10);

                value = isNaN(value) ? 0 : value;
                if (value > 0) value--;

                quantityAmount.value = value;
            }

            init();

//------- End Quantity Increase & Decrease Value --------//

            /*----------------------------------------------------*/
            /*  Google map js
              /*----------------------------------------------------*/

            if ($("#mapBox").length) {
                var $lat = $("#mapBox").data("lat");
                var $lon = $("#mapBox").data("lon");
                var $zoom = $("#mapBox").data("zoom");
                var $marker = $("#mapBox").data("marker");
                var $info = $("#mapBox").data("info");
                var $markerLat = $("#mapBox").data("mlat");
                var $markerLon = $("#mapBox").data("mlon");
                var map = new GMaps({
                    el: "#mapBox",
                    lat: $lat,
                    lng: $lon,
                    scrollwheel: false,
                    scaleControl: true,
                    streetViewControl: false,
                    panControl: true,
                    disableDoubleClickZoom: true,
                    mapTypeControl: false,
                    zoom: $zoom,
                    styles: [
                        {
                            featureType: "water",
                            elementType: "geometry.fill",
                            stylers: [
                                {
                                    color: "#dcdfe6"
                                }
                            ]
                        },
                        {
                            featureType: "transit",
                            stylers: [
                                {
                                    color: "#808080"
                                },
                                {
                                    visibility: "off"
                                }
                            ]
                        },
                        {
                            featureType: "road.highway",
                            elementType: "geometry.stroke",
                            stylers: [
                                {
                                    visibility: "on"
                                },
                                {
                                    color: "#dcdfe6"
                                }
                            ]
                        },
                        {
                            featureType: "road.highway",
                            elementType: "geometry.fill",
                            stylers: [
                                {
                                    color: "#ffffff"
                                }
                            ]
                        },
                        {
                            featureType: "road.local",
                            elementType: "geometry.fill",
                            stylers: [
                                {
                                    visibility: "on"
                                },
                                {
                                    color: "#ffffff"
                                },
                                {
                                    weight: 1.8
                                }
                            ]
                        },
                        {
                            featureType: "road.local",
                            elementType: "geometry.stroke",
                            stylers: [
                                {
                                    color: "#d7d7d7"
                                }
                            ]
                        },
                        {
                            featureType: "poi",
                            elementType: "geometry.fill",
                            stylers: [
                                {
                                    visibility: "on"
                                },
                                {
                                    color: "#ebebeb"
                                }
                            ]
                        },
                        {
                            featureType: "administrative",
                            elementType: "geometry",
                            stylers: [
                                {
                                    color: "#a7a7a7"
                                }
                            ]
                        },
                        {
                            featureType: "road.arterial",
                            elementType: "geometry.fill",
                            stylers: [
                                {
                                    color: "#ffffff"
                                }
                            ]
                        },
                        {
                            featureType: "road.arterial",
                            elementType: "geometry.fill",
                            stylers: [
                                {
                                    color: "#ffffff"
                                }
                            ]
                        },
                        {
                            featureType: "landscape",
                            elementType: "geometry.fill",
                            stylers: [
                                {
                                    visibility: "on"
                                },
                                {
                                    color: "#efefef"
                                }
                            ]
                        },
                        {
                            featureType: "road",
                            elementType: "labels.text.fill",
                            stylers: [
                                {
                                    color: "#696969"
                                }
                            ]
                        },
                        {
                            featureType: "administrative",
                            elementType: "labels.text.fill",
                            stylers: [
                                {
                                    visibility: "on"
                                },
                                {
                                    color: "#737373"
                                }
                            ]
                        },
                        {
                            featureType: "poi",
                            elementType: "labels.icon",
                            stylers: [
                                {
                                    visibility: "off"
                                }
                            ]
                        },
                        {
                            featureType: "poi",
                            elementType: "labels",
                            stylers: [
                                {
                                    visibility: "off"
                                }
                            ]
                        },
                        {
                            featureType: "road.arterial",
                            elementType: "geometry.stroke",
                            stylers: [
                                {
                                    color: "#d6d6d6"
                                }
                            ]
                        },
                        {
                            featureType: "road",
                            elementType: "labels.icon",
                            stylers: [
                                {
                                    visibility: "off"
                                }
                            ]
                        },
                        {},
                        {
                            featureType: "poi",
                            elementType: "geometry.fill",
                            stylers: [
                                {
                                    color: "#dadada"
                                }
                            ]
                        }
                    ]
                });
            }




        });

    </script>

@endsection


