@extends('template.sections.user.app')

@section('content')

    <!--employee dashboard wrapper start-->
    <div class="candidate_dashboard_wrapper jb_cover">
        <div class="container">

            <div class="row">
                <div class="col-lg-3 col-md-12 col-sm-12 col-12">
                    <div class="emp_dashboard_sidebar jb_cover">
                        <img src="{{asset('template/user/images/profile.jpg')}}" class="img-responsive" alt="post_img" />
                        <div class="emp_web_profile candidate_web_profile jb_cover">

                            <h4>{{auth('client')->user()->full_name}}</h4>
                            <p>{{auth('client')->user()->username}}</p>
                            <div class="skills jb_cover">
                                <div class="skill-item jb_cover">
                                    <h6>پروفایل<span>30%</span></h6>
                                    <div class="skills-progress"><span data-value="30%"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="emp_follow_link jb_cover">
                            <ul class="feedlist">
                                <li><a href="{{route('client.dashboard')}}" class="link_active"><i class="fas fa-tachometer-alt"></i> داشبورد </a></li>
                                <li>
                                    <a href="{{route('client.edit')}}"> <i class="fas fa-edit"></i>ویرایش پروفایل
                                    </a>
                                </li>
                                @if(hasEmployer(auth('client')->user()))

                                    <li><a href="#"><i class="fas fa-file"></i>رزومه </a></li>

                                @elseif(hasSeeker(auth('client')->user()))

                                    <li><a href="#"><i class="fas fa-file"></i>آگهی ها </a></li>

                                    <li><a href="#"><i class="fas fa-file"></i>پلن ها </a></li>

                                    <li><a href="#"><i class="fas fa-file"></i>کارجویان  </a></li>

                                @endif

                            </ul>
                            <ul class="feedlist logout_link jb_cover">
                                <li><a href="{{route('client.logout.panel')}}"><i class="fas fa-power-off"></i> خروج  </a></li>

                            </ul>
                        </div>
                    </div>
                    <div class="modal fade delete_popup" id="myModal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">

                                        <div class="delett_cntn jb_cover">
                                            <h1><i class="fas fa-trash-alt"></i> حذف حساب</h1>
                                            <p>شما مطمئن هستید! شما میخواهید پروفایل خود را حذف کنید.
                                                <br> نمی تواند برگردانده شود!</p>

                                            <div class="delete_jb_form">

                                                <input type="password" name="password" placeholder="رمز عبور را وارد کنید">
                                            </div>
                                            <div class="header_btn search_btn applt_pop_btn">

                                                <a href="#">ذخیره به روزرسانی</a>

                                            </div>
                                            <div class="cancel_wrapper">
                                                <a href="#" class="" data-dismiss="modal">لغو</a>
                                            </div>
                                            <div class="login_remember_box jb_cover">
                                                <label class="control control--checkbox">شما شرایط و ضوابط <a href="#">و حریم خصوصی </a> ما را <a href="#">میپذیرید</a>
                                                    <input type="checkbox">
                                                    <span class="control__indicator"></span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="manage_jobs_wrapper jb_cover">
                                <div class="job_list mange_list applications_recent">

                                    <h6>06 مورد علاقه</h6>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="jb_listing_left_fullwidth mt-0 jb_cover">
                                <div class="row">
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-12">
                                        <div class="jb_job_post_side_img">
                                            <img src="{{asset('template/user/images/lt1.png')}}" alt="post_img" />
                                            <br> <span>گوگل</span>
                                        </div>
                                        <div class="jb_job_post_right_cont">
                                            <h4><a href="#">کارآموز طراح وب، (تازه)</a></h4>

                                            <ul>
                                                <li><i class="flaticon-cash"></i>&nbsp; 12هزار - 15هزار تومان</li>
                                                <li><i class="flaticon-location-pointer"></i>&nbsp; لس آنجلس، کالیفرنیا</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                                        <div class="jb_job_post_right_btn_wrapper">
                                            <ul>
                                                <li>
                                                    <div class="job_adds_right">
                                                        <a href="#"><i class="far fa-heart"></i></a>
                                                    </div>
                                                </li>
                                                <li><a href="#">نیمه وقت</a></li>
                                                <li> <a href="#" data-toggle="modal" data-target="#myModal6">درخواست</a></li>
                                            </ul>
                                        </div>
                                        <div class="modal fade apply_job_popup" id="myModal6" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">

                                                            <div class="apply_job jb_cover">
                                                                <h1>درخواست برای این کار :</h1>
                                                                <div class="search_alert_box jb_cover">

                                                                    <div class="apply_job_form">

                                                                        <input type="text" name="name" placeholder="نام کامل">
                                                                    </div>
                                                                    <div class="apply_job_form">

                                                                        <input type="text" name="Email" placeholder="ایمیل خود را وارد کنید">
                                                                    </div>
                                                                    <div class="apply_job_form">
                                                                        <textarea class="form-control" name="message" placeholder="پیام"></textarea>
                                                                    </div>

                                                                    <div class="resume_optional jb_cover">
                                                                        <p>رزومه (اختیاری)</p>
                                                                        <div class="width_50">
                                                                            <input type="file" id="input-file-now-custom-6" class="dropify" data-height="90" /><span class="post_photo">آپلود رزومه</span>
                                                                        </div>
                                                                        <p class="word_file"> مایکروسافت ورد یا فایل پی دی اف فقط (5 مگابایت)</p>
                                                                    </div>

                                                                </div>
                                                                <div class="header_btn search_btn applt_pop_btn jb_cover ajt">

                                                                    <a href="#">درخواست حالا</a>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="jb_listing_left_fullwidth jb_cover">
                                <div class="row">
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-12">
                                        <div class="jb_job_post_side_img">
                                            <img src="{{asset('template/user/images/lt2.png')}}" alt="post_img" />
                                            <br> <span>گوگل</span>
                                        </div>
                                        <div class="jb_job_post_right_cont">
                                            <h4><a href="#">مهندس نرم افزار، (تازه)</a></h4>

                                            <ul>
                                                <li><i class="flaticon-cash"></i>&nbsp; 12هزار - 15هزار تومان</li>
                                                <li><i class="flaticon-location-pointer"></i>&nbsp; لس آنجلس، کالیفرنیا</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                                        <div class="jb_job_post_right_btn_wrapper">
                                            <ul>
                                                <li>
                                                    <div class="job_adds_right">
                                                        <a href="#"><i class="far fa-heart"></i></a>
                                                    </div>
                                                </li>
                                                <li><a href="#">نیمه وقت</a></li>
                                                <li> <a href="#" data-toggle="modal" data-target="#myModal3">درخواست</a></li>
                                            </ul>
                                        </div>
                                        <div class="modal fade apply_job_popup" id="myModal3" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">

                                                            <div class="apply_job jb_cover">
                                                                <h1>درخواست برای این کار :</h1>
                                                                <div class="search_alert_box jb_cover">

                                                                    <div class="apply_job_form">

                                                                        <input type="text" name="name" placeholder="نام کامل">
                                                                    </div>
                                                                    <div class="apply_job_form">

                                                                        <input type="text" name="Email" placeholder="ایمیل خود را وارد کنید">
                                                                    </div>
                                                                    <div class="apply_job_form">
                                                                        <textarea class="form-control" name="message" placeholder="پیام"></textarea>
                                                                    </div>

                                                                    <div class="resume_optional jb_cover">
                                                                        <p>رزومه (اختیاری)</p>
                                                                        <div class="width_50">
                                                                            <input type="file" id="input-file-now-custom-2" class="dropify" data-height="90" /><span class="post_photo">آپلود رزومه</span>
                                                                        </div>
                                                                        <p class="word_file"> مایکروسافت ورد یا فایل پی دی اف فقط (5 مگابایت)</p>
                                                                    </div>

                                                                </div>
                                                                <div class="header_btn search_btn applt_pop_btn jb_cover ajt">

                                                                    <a href="#">درخواست حالا</a>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="jb_listing_left_fullwidth jb_cover">
                                <div class="row">
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-12">
                                        <div class="jb_job_post_side_img">
                                            <img src="{{asset('template/user/images/lt3.png')}}" alt="post_img" />
                                            <br> <span>گوگل</span>
                                        </div>
                                        <div class="jb_job_post_right_cont">
                                            <h4><a href="#">کارآموز طراح وب، (تازه)</a></h4>

                                            <ul>
                                                <li><i class="flaticon-cash"></i>&nbsp; 12هزار - 15هزار تومان</li>
                                                <li><i class="flaticon-location-pointer"></i>&nbsp; لس آنجلس، کالیفرنیا</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                                        <div class="jb_job_post_right_btn_wrapper">
                                            <ul>
                                                <li>
                                                    <div class="job_adds_right">
                                                        <a href="#"><i class="far fa-heart"></i></a>
                                                    </div>
                                                </li>
                                                <li><a href="#">نیمه وقت</a></li>
                                                <li> <a href="#" data-toggle="modal" data-target="#myModal16">درخواست</a></li>
                                            </ul>
                                        </div>
                                        <div class="modal fade apply_job_popup" id="myModal16" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">

                                                            <div class="apply_job jb_cover">
                                                                <h1>درخواست برای این کار :</h1>
                                                                <div class="search_alert_box jb_cover">

                                                                    <div class="apply_job_form">

                                                                        <input type="text" name="name" placeholder="نام کامل">
                                                                    </div>
                                                                    <div class="apply_job_form">

                                                                        <input type="text" name="Email" placeholder="ایمیل خود را وارد کنید">
                                                                    </div>
                                                                    <div class="apply_job_form">
                                                                        <textarea class="form-control" name="message" placeholder="پیام"></textarea>
                                                                    </div>

                                                                    <div class="resume_optional jb_cover">
                                                                        <p>رزومه (اختیاری)</p>
                                                                        <div class="width_50">
                                                                            <input type="file" id="input-file-now-custom-16" class="dropify" data-height="90" /><span class="post_photo">آپلود رزومه</span>
                                                                        </div>
                                                                        <p class="word_file"> مایکروسافت ورد یا فایل پی دی اف فقط (5 مگابایت)</p>
                                                                    </div>

                                                                </div>
                                                                <div class="header_btn search_btn applt_pop_btn jb_cover ajt">

                                                                    <a href="#">درخواست حالا</a>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="jb_listing_left_fullwidth jb_cover">
                                <div class="row">
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-12">
                                        <div class="jb_job_post_side_img">
                                            <img src="{{asset('template/user/images/lt4.png')}}" alt="post_img" />
                                            <br> <span>گوگل</span>
                                        </div>
                                        <div class="jb_job_post_right_cont">
                                            <h4><a href="#">کارآموز طراح وب، (تازه)</a></h4>

                                            <ul>
                                                <li><i class="flaticon-cash"></i>&nbsp; 12هزار - 15هزار تومان</li>
                                                <li><i class="flaticon-location-pointer"></i>&nbsp; لس آنجلس، کالیفرنیا</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                                        <div class="jb_job_post_right_btn_wrapper">
                                            <ul>
                                                <li>
                                                    <div class="job_adds_right">
                                                        <a href="#"><i class="far fa-heart"></i></a>
                                                    </div>
                                                </li>
                                                <li><a href="#">نیمه وقت</a></li>
                                                <li> <a href="#" data-toggle="modal" data-target="#myModal61">درخواست</a></li>
                                            </ul>
                                        </div>
                                        <div class="modal fade apply_job_popup" id="myModal61" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">

                                                            <div class="apply_job jb_cover">
                                                                <h1>درخواست برای این کار :</h1>
                                                                <div class="search_alert_box jb_cover">

                                                                    <div class="apply_job_form">

                                                                        <input type="text" name="name" placeholder="نام کامل">
                                                                    </div>
                                                                    <div class="apply_job_form">

                                                                        <input type="text" name="Email" placeholder="ایمیل خود را وارد کنید">
                                                                    </div>
                                                                    <div class="apply_job_form">
                                                                        <textarea class="form-control" name="message" placeholder="پیام"></textarea>
                                                                    </div>

                                                                    <div class="resume_optional jb_cover">
                                                                        <p>رزومه (اختیاری)</p>
                                                                        <div class="width_50">
                                                                            <input type="file" id="input-file-now-custom-61" class="dropify" data-height="90" /><span class="post_photo">آپلود رزومه</span>
                                                                        </div>
                                                                        <p class="word_file"> مایکروسافت ورد یا فایل پی دی اف فقط (5 مگابایت)</p>
                                                                    </div>

                                                                </div>
                                                                <div class="header_btn search_btn applt_pop_btn jb_cover ajt">

                                                                    <a href="#">درخواست حالا</a>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="jb_listing_left_fullwidth jb_cover">
                                <div class="row">
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-12">
                                        <div class="jb_job_post_side_img">
                                            <img src="{{asset('template/user/images/lt4.png')}}" alt="post_img" />
                                            <br> <span>گوگل</span>
                                        </div>
                                        <div class="jb_job_post_right_cont">
                                            <h4><a href="#">مهندس نرم افزار، (تازه)</a></h4>

                                            <ul>
                                                <li><i class="flaticon-cash"></i>&nbsp; 12هزار - 15هزار تومان</li>
                                                <li><i class="flaticon-location-pointer"></i>&nbsp; لس آنجلس، کالیفرنیا</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                                        <div class="jb_job_post_right_btn_wrapper">
                                            <ul>
                                                <li>
                                                    <div class="job_adds_right">
                                                        <a href="#"><i class="far fa-heart"></i></a>
                                                    </div>
                                                </li>
                                                <li><a href="#">نیمه وقت</a></li>
                                                <li> <a href="#" data-toggle="modal" data-target="#myModal0">درخواست</a></li>
                                            </ul>
                                        </div>
                                        <div class="modal fade apply_job_popup" id="myModal0" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">

                                                            <div class="apply_job jb_cover">
                                                                <h1>درخواست برای این کار :</h1>
                                                                <div class="search_alert_box jb_cover">

                                                                    <div class="apply_job_form">

                                                                        <input type="text" name="name" placeholder="نام کامل">
                                                                    </div>
                                                                    <div class="apply_job_form">

                                                                        <input type="text" name="Email" placeholder="ایمیل خود را وارد کنید">
                                                                    </div>
                                                                    <div class="apply_job_form">
                                                                        <textarea class="form-control" name="message" placeholder="پیام"></textarea>
                                                                    </div>

                                                                    <div class="resume_optional jb_cover">
                                                                        <p>رزومه (اختیاری)</p>
                                                                        <div class="width_50">
                                                                            <input type="file" id="input-file-now-custom-0" class="dropify" data-height="90" /><span class="post_photo">آپلود رزومه</span>
                                                                        </div>
                                                                        <p class="word_file"> مایکروسافت ورد یا فایل پی دی اف فقط (5 مگابایت)</p>
                                                                    </div>

                                                                </div>
                                                                <div class="header_btn search_btn applt_pop_btn jb_cover ajt">

                                                                    <a href="#">درخواست حالا</a>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="jb_listing_left_fullwidth jb_cover">
                                <div class="row">
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-12">
                                        <div class="jb_job_post_side_img">
                                            <img src="{{asset('template/user/images/lt2.png')}}" alt="post_img" />
                                            <br> <span>گوگل</span>
                                        </div>
                                        <div class="jb_job_post_right_cont">
                                            <h4><a href="#">کارآموز طراح وب، (تازه)</a></h4>

                                            <ul>
                                                <li><i class="flaticon-cash"></i>&nbsp; 12هزار - 15هزار تومان</li>
                                                <li><i class="flaticon-location-pointer"></i>&nbsp; لس آنجلس، کالیفرنیا</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                                        <div class="jb_job_post_right_btn_wrapper">
                                            <ul>
                                                <li>
                                                    <div class="job_adds_right">
                                                        <a href="#"><i class="far fa-heart"></i></a>
                                                    </div>
                                                </li>
                                                <li><a href="#">نیمه وقت</a></li>
                                                <li> <a href="#" data-toggle="modal" data-target="#myModal22">درخواست</a></li>
                                            </ul>
                                        </div>
                                        <div class="modal fade apply_job_popup" id="myModal22" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">

                                                            <div class="apply_job jb_cover">
                                                                <h1>درخواست برای این کار :</h1>
                                                                <div class="search_alert_box jb_cover">

                                                                    <div class="apply_job_form">

                                                                        <input type="text" name="name" placeholder="نام کامل">
                                                                    </div>
                                                                    <div class="apply_job_form">

                                                                        <input type="text" name="Email" placeholder="ایمیل خود را وارد کنید">
                                                                    </div>
                                                                    <div class="apply_job_form">
                                                                        <textarea class="form-control" name="message" placeholder="پیام"></textarea>
                                                                    </div>

                                                                    <div class="resume_optional jb_cover">
                                                                        <p>رزومه (اختیاری)</p>
                                                                        <div class="width_50">
                                                                            <input type="file" id="input-file-now-custom-22" class="dropify" data-height="90" /><span class="post_photo">آپلود رزومه</span>
                                                                        </div>
                                                                        <p class="word_file"> مایکروسافت ورد یا فایل پی دی اف فقط (5 مگابایت)</p>
                                                                    </div>

                                                                </div>
                                                                <div class="header_btn search_btn applt_pop_btn jb_cover ajt">

                                                                    <a href="#">درخواست حالا</a>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--employee dashboard wrapper end-->


@endsection

