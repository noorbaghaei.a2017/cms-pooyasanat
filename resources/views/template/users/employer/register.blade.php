@extends('template.app')
@section('content')

    <!-- navi wrapper End -->

    <!-- login wrapper start -->
    <div class="login_wrapper jb_cover">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="login_top_box jb_cover">
                        <div class="login_banner_wrapper">
                            <img src="{{asset('template/images/logo.png')}}" alt="logo">
                            <div class="header_btn search_btn facebook_wrap jb_cover">

                                <a href="#">ورود با فیسبوک <i class="fab fa-facebook-f"></i></a>

                            </div>
                            <div class="header_btn search_btn google_wrap jb_cover">

                                <a href="#">ورود با پینترست <i class="fab fa-pinterest-p"></i></a>

                            </div>
                            <div class="jp_regis_center_tag_wrapper jb_register_red_or">
                                <h1>یا</h1>
                            </div>
                        </div>

                        <form action="{{ route('client.register.submit.employer') }}" method="POST" class="login100-form validate-form p-l-55 p-r-55 p-t-178" style="direction: rtl">

                            @csrf

                            <div class="login_form_wrapper">

                                <h2>ورود</h2>
                                @error('first_name')
                                <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div class="form-group icon_form comments_form">
                                    <input type="text" class="form-control require" name="first_name" placeholder="نام *" autocomplete="off">
                                    <i class="fas fa-user"></i>
                                </div>
                                @error('last_name')
                                <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                    <div class="form-group icon_form comments_form">

                                    <input type="text" class="form-control require" name="last_name" placeholder="نام خانوادگی*" autocomplete="off">
                                    <i class="fas fa-user"></i>
                                    </div>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <div class="form-group icon_form comments_form">
                                    <input type="text" class="form-control require" name="email" placeholder="آدرس ایمیل*">
                                    <i class="fas fa-envelope"></i>
                                </div>
                                @error('password')
                                <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div class="form-group icon_form comments_form">

                                    <input type="password" class="form-control require" name="password" placeholder="رمزعبور *">
                                    <i class="fas fa-lock"></i>
                                </div>

                                <div class="header_btn search_btn login_btn jb_cover">

                                    <input type="submit" value="ثبت نام">
                                </div>
                                <div class="dont_have_account jb_cover">
                                    <p>حساب کاربری دارید؟ <a href="{{route('client.dashboard')}}">ورود</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- login wrapper end -->

@endsection



